package com.rolls_royce.events.zpass.main.manual_check_in.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.main.manual_registration.model.ManualSoloRegistrationRequestModel;
import com.rolls_royce.events.zpass.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public interface ManualCheckInApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_MANUAL_CHECK_IN)
    Call<BaseResponseModel> checkInUser(@Header("Authorization") String authToken,
                                             @Body ManualCheckInRequestModel body);
}
