package com.rolls_royce.events.zpass.data;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.EventModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by LloydM on 10/11/17
 * for Livefitter
 */

public interface EventActivitiesApiMethods {

    @GET(AppConstants.URL_EVENTS)
    Call<BaseResponseModel<ArrayList<EventModel>>> retrieveEvents(@Header("Authorization") String authToken);

    @GET(AppConstants.URL_ACTIVITIES)
    Call<BaseResponseModel<ArrayList<EventActivityModel>>> retrieveActivitiesForEvent(@Header("Authorization") String authToken,
                                                                                      @Query("event_id") int eventId);

    @GET(AppConstants.URL_ACTIVITIES + "/{activityId}")
    Call<BaseResponseModel<EventActivityModel>> retrieveActivity(@Header("Authorization") String authToken,
                                                                 @Path("activityId") int activityId);
}
