package com.rolls_royce.events.zpass.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.UserModel;


/**
 * Created by LloydM on 9/29/17
 * for Livefitter
 */

public class SharedPrefsUtil {

    private static final String TAG = SharedPrefsUtil.class.getSimpleName();

    public static void saveCurrentUser(Context context, UserModel userModel) {
        Log.d(TAG, "saveCurrentUser : " + userModel);
        String encodedUser = UserModel.encode(userModel);

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_USER, Context.MODE_PRIVATE).edit();

        editor.putString(AppConstants.PREFS_CURRENT_USER, encodedUser).apply();
    }

    public static UserModel getCurrentUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(AppConstants.PREFS_USER, Context.MODE_PRIVATE);
        String userModelToDecode = prefs.getString(AppConstants.PREFS_CURRENT_USER, null);

        if(!Strings.isNullOrEmpty(userModelToDecode)){
            UserModel userModel = UserModel.decode(userModelToDecode);
            Log.d(TAG, "getCurrentUser: " + userModel);
            return userModel;
        }

        Log.d(TAG, "getCurrentUser null: ");
        return null;
    }

    public static void clearUserPrefs(Context context) {
        Log.d(TAG, "clearUserPrefs");

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_USER, Context.MODE_PRIVATE).edit();

        editor.clear().apply();
    }
}
