package com.rolls_royce.events.zpass.main.log_in.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;
import com.rolls_royce.events.zpass.model.UserModel;


/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public interface LogInService {
    void logIn(LogInRequestModel body,
               final Callback callback);

    public interface Callback extends BaseServiceCallback {

        void onSuccess(UserModel userModel);

        void onError(@Nullable String message);
    }
}