package com.rolls_royce.events.zpass.main.event_list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.main.event_list.EventListContract;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class EventItemViewHolder extends RecyclerView.ViewHolder implements EventListContract.RepositoryItemView {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_date)
    TextView tvDate;

    public static EventItemViewHolder create(LayoutInflater inflater, ViewGroup parent, View.OnClickListener clickListener){
        View view = inflater.inflate(R.layout.item_event_list, parent, false);
        return new EventItemViewHolder(view, clickListener);
    }

    private EventItemViewHolder(View itemView, View.OnClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(clickListener);
    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setDate(String date) {
        tvDate.setText(date);
    }
}
