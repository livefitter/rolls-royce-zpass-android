package com.rolls_royce.events.zpass.utility;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;


/**
 * Convenience class for the common operation of clearing the error
 * for the {@link TextInputLayout} when the user types anything to the {@link android.support.design.widget.TextInputEditText} that this watcher is attached to.
 *
 * Also for convenience, this sets {@link TextInputLayout#setErrorEnabled(boolean)} to false to remove the errorView that appears at the bottom
 * after setting {@link TextInputLayout#setError(CharSequence)} to null
 */

public class FormFieldTextWatcher implements TextWatcher {

    private TextInputLayout mTextInputLayout;

    public FormFieldTextWatcher(TextInputLayout textInputLayout) {
        this.mTextInputLayout = textInputLayout;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if(mTextInputLayout != null){
            mTextInputLayout.setError(null);
            mTextInputLayout.setErrorEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
