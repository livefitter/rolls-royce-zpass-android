package com.rolls_royce.events.zpass.main.attendance;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.EventActivityModel;

/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public interface AttendanceContract {

    public interface Presenter extends BasePresenter {
        EventActivityModel getActivityModel();
        void retrieveActivityDetails();
    }

    public interface View extends BaseView<Presenter> {
        void toggleProgressVisibility(boolean isVisible);
        void setActivityModel(EventActivityModel activityModel);
        void showActivityRetrievalError(String message);
        void requestActivityDataRefresh();
    }

    public interface TabItemView{
        void setActivityModel(EventActivityModel activityModel);
        void setQuery(String query);
    }
}
