package com.rolls_royce.events.zpass.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by LloydM on 10/11/17
 * for Livefitter
 */

public class EventActivityModel implements Parcelable {


    /**
     *       id: 1,
             image_url: "some-URL",
             name: "Tug of War",
             slots: 100,
             description: "some-text",
             venue: "Open Field",
             available_slots: 100,
             date_time: "29 Oct 2017, 01:00PM - 02:00PM",
             is_team_activity: true,
             number_of_members_needed: 10,
             survey: "some-long-text",
             survey_title: "some-long-text",
             survey_is_mandatory: true,
             survey_image_url: "some-other-URL",
             is_registered: true,
             activity_date: "30 October 2017",
             activity_time: "01:00 PM - 02:00 PM",
             activity_cut_off_date: "15 October 2017",
             total_scan: 10,
             participants: [],
             houses: []
     */

    private int id;
    @SerializedName("image_url")
    private String imageUrl;
    private String name;
    private int slots;
    private String description;
    private String venue;
    @SerializedName("available_slots")
    private int availableSlots;
    @SerializedName("date_time")
    private String dateTime;
    @SerializedName("is_team_activity")
    private boolean isTeamActivity;
    @SerializedName("number_of_members_needed")
    private int requiredMemberCount;
    @SerializedName("survey")
    private String surveyText;
    @SerializedName("survey_title")
    private String surveyTitle;
    @SerializedName("survey_is_mandatory")
    private boolean isSurveyMandatory;
    @SerializedName("survey_image_url")
    private String surveyImageUrl;
    @SerializedName("is_registered")
    private boolean isRegistered;
    @SerializedName("activity_date")
    private String activityDate;
    @SerializedName("activity_time")
    private String activityTime;
    @SerializedName("activity_cut_off_date")
    private String activityCutOffDate;
    @SerializedName("total_scan")
    private int totalScanned;
    @SerializedName("total_walkins")
    private int totalWalkIns;
    @SerializedName("number_of_winners")
    private int numberOfWinners;
    private ArrayList<ParticipantModel> participants;
    private ArrayList<ActivityTeamModel> teams;

    //internal variables
    private boolean isPastActivty;

    protected EventActivityModel(Parcel in) {
        id = in.readInt();
        imageUrl = in.readString();
        name = in.readString();
        slots = in.readInt();
        description = in.readString();
        venue = in.readString();
        availableSlots = in.readInt();
        dateTime = in.readString();
        isTeamActivity = in.readByte() != 0;
        requiredMemberCount = in.readInt();
        surveyText = in.readString();
        surveyTitle = in.readString();
        isSurveyMandatory = in.readByte() != 0;
        surveyImageUrl = in.readString();
        isRegistered = in.readByte() != 0;
        activityDate = in.readString();
        activityTime = in.readString();
        activityCutOffDate = in.readString();
        totalScanned = in.readInt();
        totalWalkIns = in.readInt();
        numberOfWinners = in.readInt();
        participants = in.createTypedArrayList(ParticipantModel.CREATOR);
        teams = in.createTypedArrayList(ActivityTeamModel.CREATOR);

        isPastActivty = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(imageUrl);
        dest.writeString(name);
        dest.writeInt(slots);
        dest.writeString(description);
        dest.writeString(venue);
        dest.writeInt(availableSlots);
        dest.writeString(dateTime);
        dest.writeByte((byte) (isTeamActivity ? 1 : 0));
        dest.writeInt(requiredMemberCount);
        dest.writeString(surveyText);
        dest.writeString(surveyTitle);
        dest.writeByte((byte) (isSurveyMandatory ? 1 : 0));
        dest.writeString(surveyImageUrl);
        dest.writeByte((byte) (isRegistered ? 1 : 0));
        dest.writeString(activityDate);
        dest.writeString(activityTime);
        dest.writeString(activityCutOffDate);
        dest.writeInt(totalScanned);
        dest.writeInt(totalWalkIns);
        dest.writeInt(numberOfWinners);
        dest.writeTypedList(participants);
        dest.writeTypedList(teams);

        dest.writeByte((byte) (isPastActivty ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventActivityModel> CREATOR = new Creator<EventActivityModel>() {
        @Override
        public EventActivityModel createFromParcel(Parcel in) {
            return new EventActivityModel(in);
        }

        @Override
        public EventActivityModel[] newArray(int size) {
            return new EventActivityModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public int getSlots() {
        return slots;
    }

    public String getDescription() {
        return description;
    }

    public String getVenue() {
        return venue;
    }

    public int getAvailableSlots() {
        return availableSlots;
    }

    public String getDateTime() {
        return dateTime;
    }

    public boolean isTeamActivity() {
        return isTeamActivity;
    }

    public int getRequiredMemberCount() {
        return requiredMemberCount;
    }

    public String getSurveyText() {
        return surveyText;
    }

    public String getSurveyTitle() {
        return surveyTitle;
    }

    public boolean isSurveyMandatory() {
        return isSurveyMandatory;
    }

    public String getSurveyImageUrl() {
        return surveyImageUrl;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public String getActivityCutOffDate() {
        return activityCutOffDate;
    }

    public int getTotalScanned() {
        return totalScanned;
    }

    public int getTotalWalkIns() {
        return totalWalkIns;
    }

    public int getNumberOfWinners() {
        return numberOfWinners;
    }

    public ArrayList<ParticipantModel> getParticipants() {
        return participants;
    }

    public ArrayList<ActivityTeamModel> getTeams() {
        return teams;
    }

    public boolean isPastActivity() {
        return isPastActivty;
    }

    public void setPastActivity(boolean pastActivty) {
        isPastActivty = pastActivty;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }
}
