package com.rolls_royce.events.zpass.main.attendance.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.attendance.AttendanceContract;
import com.rolls_royce.events.zpass.main.attendance.adapter.AttendancePagerAdapter;
import com.rolls_royce.events.zpass.main.attendance.presenter.AttendancePresenterImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public class AttendanceActivity extends BaseActivity implements AttendanceContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout mProgressBarLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private AttendancePagerAdapter mAdapter;
    private AttendanceContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_attendance, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                handleSearchQuery(null);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                onSearchRequested();
                return true;
            case R.id.action_log_out:
                showLogOutDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (Intent.ACTION_SEARCH.equals(intent.getAction()) && intent.hasExtra(SearchManager.QUERY)) {
            handleSearchQuery(intent.getStringExtra(SearchManager.QUERY));
        }

    }

    private void handleSearchQuery(@Nullable String query){
        if (mAdapter != null) {
            query = Strings.nullToEmpty(query).trim();
            LoggingHelper.debug(AttendanceActivity.this, "handleSearchQuery", "query: " + query);
            mAdapter.setQueryToChild(mViewPager.getCurrentItem(), query);
        }
    }

    private void initialize() {
        String title = getString(R.string.label_attendance).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressBarLayout.setBackgroundResource(R.color.white);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mProgressBarLayout.getLayoutParams();
        layoutParams.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        mProgressBarLayout.setLayoutParams(layoutParams);
        mProgressBarLayout.requestLayout();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.BUNDLE_ACTIVITY)) {
            UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
            EventActivityModel activityModel = extras.getParcelable(AppConstants.BUNDLE_ACTIVITY);

            if (userModel != null && activityModel != null) {

                new AttendancePresenterImpl(userModel, activityModel, this);

            }
        }

        if (mPresenter != null) {

            mAdapter = new AttendancePagerAdapter(this, getSupportFragmentManager(), mPresenter);

            mViewPager.setAdapter(mAdapter);

            mTabLayout.setupWithViewPager(mViewPager);

            mPresenter.start();
        }
    }

    @Override
    public void setPresenter(AttendanceContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        if (mProgressBarLayout != null) {
            int visibility = isVisible? View.VISIBLE : View.GONE;
            mProgressBarLayout.setVisibility(visibility);
        }
    }

    @Override
    public void setActivityModel(EventActivityModel activityModel) {
        if (mAdapter != null) {
            mAdapter.setActivityModelToChild(activityModel);
        }
    }

    @Override
    public void showActivityRetrievalError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_retrieve_activity_details);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void requestActivityDataRefresh() {
        if (mPresenter != null) {
            mPresenter.retrieveActivityDetails();
        }
    }
}
