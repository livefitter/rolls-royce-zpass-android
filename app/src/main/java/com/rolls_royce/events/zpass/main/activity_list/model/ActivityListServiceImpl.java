package com.rolls_royce.events.zpass.main.activity_list.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.data.EventActivitiesApiMethods;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class ActivityListServiceImpl implements ActivityListService {
    private EventActivitiesApiMethods mApiMethods;

    private void initialize() {
        if (mApiMethods == null) {
            mApiMethods = RetrofitUtil.getGSONRetrofit().create(EventActivitiesApiMethods.class);
        }
    }

    @Override
    public void retrieveActivities(String authToken, int eventId, final RetrieveActivityListCallback callback) {

        initialize();
        mApiMethods.retrieveActivitiesForEvent(authToken, eventId)
                .enqueue(new Callback<BaseResponseModel<ArrayList<EventActivityModel>>>() {
                    @Override
                    public void onResponse(Call<BaseResponseModel<ArrayList<EventActivityModel>>> call, Response<BaseResponseModel<ArrayList<EventActivityModel>>> response) {
                        LoggingHelper.debug(this, "onResponse", response);

                        if (response.isSuccessful()) {
                            if (response.body().getStatus()) {
                                LoggingHelper.debug(this, "onResponse", "status true data: " + response.body().getData());
                                callback.onRetrieveActivitySuccess(response.body().getData());
                            } else {
                                String message = response.body().getMessage();
                                LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                                if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                                    callback.onInvalidAuthenticationError(message);
                                } else {
                                    callback.onRetrieveActivityError(message);
                                }
                            }
                        } else {
                            LoggingHelper.debug(this, "onResponse", "failed");
                            callback.onRetrieveActivityError(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponseModel<ArrayList<EventActivityModel>>> call, Throwable t) {
                        t.printStackTrace();
                        callback.onRetrieveActivityError(null);
                    }
                });
    }
}
