package com.rolls_royce.events.zpass.main.event_list.presenter;

import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.main.event_list.EventListContract;
import com.rolls_royce.events.zpass.main.event_list.model.EventListService;
import com.rolls_royce.events.zpass.main.event_list.model.EventListServiceImpl;
import com.rolls_royce.events.zpass.model.EventModel;
import com.rolls_royce.events.zpass.model.UserModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class EventListPresenterImpl implements EventListContract.Presenter, EventListService.RetrieveEventListCallback {

    private EventListContract.View mView;
    private EventListService mService;
    private final UserModel mUserModel;
    private ArrayList<EventModel> mEventList;

    private boolean isInitialLoad = true;

    public EventListPresenterImpl(UserModel userModel, EventListContract.View view) {
        this.mUserModel = userModel;
        this.mView = view;
        this.mService = new EventListServiceImpl();
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        retrieveEventList();
    }

    @Override
    public void retrieveEventList() {
        if (isInitialLoad) {
            mView.toggleProgressVisibility(true);
        }

        mService.retrieveEvents(mUserModel.getAuthToken(), this);
    }

    @Override
    public int getEventCount() {
        return mEventList != null ? mEventList.size() : 0;
    }

    @Override
    public void bindItemView(EventListContract.RepositoryItemView itemView, int index) {
        Preconditions.checkElementIndex(index, mEventList.size());
        EventModel eventModel = mEventList.get(index);

        if (!Strings.isNullOrEmpty(eventModel.getName())) {
            itemView.setName(eventModel.getName());
        }

        if (!Strings.isNullOrEmpty(eventModel.getDateTime())) {
            itemView.setDate(eventModel.getDateTime());
        }
    }

    @Override
    public void onEventItemClick(int index) {
        Preconditions.checkElementIndex(index, mEventList.size());
        EventModel eventModel = mEventList.get(index);

        if (eventModel.getId() != 0) {
            mView.proceedToActivityListScreen(eventModel.getId());
        }
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onRetrieveEventsSuccess(ArrayList<EventModel> eventList) {
        mEventList = eventList;
        isInitialLoad = false;

        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleSwipeProgress(false);

            mView.toggleUIEnabled(!mEventList.isEmpty());
            mView.updateEventList();
        }
    }

    @Override
    public void onRetrieveEventsError(@Nullable String message) {

        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleSwipeProgress(false);
            mView.showRetrieveEventListError(message);
        }
    }
}