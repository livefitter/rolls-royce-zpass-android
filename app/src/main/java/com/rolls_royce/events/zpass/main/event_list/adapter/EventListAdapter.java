package com.rolls_royce.events.zpass.main.event_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rolls_royce.events.zpass.main.event_list.EventListContract;


/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class EventListAdapter extends RecyclerView.Adapter<EventItemViewHolder> {

    private final View.OnClickListener mClickListener;
    LayoutInflater mInflater;
    EventListContract.Presenter mPresenter;

    public EventListAdapter(Context context, EventListContract.Presenter presenter, View.OnClickListener clickListener){
        mInflater = LayoutInflater.from(context);
        this.mPresenter = presenter;
        this.mClickListener = clickListener;
    }

    @Override
    public EventItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return EventItemViewHolder.create(mInflater, parent, mClickListener);
    }

    @Override
    public void onBindViewHolder(EventItemViewHolder holder, int position) {
        if (mPresenter != null) {
            mPresenter.bindItemView(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mPresenter != null? mPresenter.getEventCount() : 0;
    }
}
