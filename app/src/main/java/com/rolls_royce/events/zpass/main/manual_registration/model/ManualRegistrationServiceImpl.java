package com.rolls_royce.events.zpass.main.manual_registration.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualRegistrationServiceImpl implements ManualRegistrationService {
    private ManualRegistrationApiMethods mApiMethods;

    private void initialize() {
        if (mApiMethods == null) {
            mApiMethods = RetrofitUtil.getGSONRetrofit().create(ManualRegistrationApiMethods.class);
        }
    }

    @Override
    public void soloRegistration(String authToken, ManualSoloRegistrationRequestModel body, final Callback callback) {
        initialize();

        mApiMethods.soloRegistration(authToken, body).enqueue(new retrofit2.Callback<BaseResponseModel>() {
            @Override
            public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true message: " + response.body().getMessage());
                        callback.onRegistrationSuccess(response.body().getMessage());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onRegistrationError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onRegistrationError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onRegistrationError(null);
            }
        });
    }

    @Override
    public void teamRegistration(String authToken, ManualTeamRegistrationRequestModel body, final Callback callback) {
        initialize();

        mApiMethods.teamRegistration(authToken, body).enqueue(new retrofit2.Callback<BaseResponseModel>() {
            @Override
            public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true message: " + response.body().getMessage());
                        callback.onRegistrationSuccess(response.body().getMessage());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onRegistrationError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onRegistrationError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onRegistrationError(null);
            }
        });
    }
}
