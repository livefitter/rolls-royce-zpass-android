package com.rolls_royce.events.zpass.main.attendees_list.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseFragment;
import com.rolls_royce.events.zpass.main.attendance.AttendanceContract;
import com.rolls_royce.events.zpass.main.attendance.view.AttendanceActivity;
import com.rolls_royce.events.zpass.main.attendees_list.AttendeesListContract;
import com.rolls_royce.events.zpass.main.attendees_list.adapter.AttendeesListAdapter;
import com.rolls_royce.events.zpass.main.attendees_list.presenter.AttendeesListPresenterImpl;
import com.rolls_royce.events.zpass.main.scan_attendance.view.ScanAttendanceActivity;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public class AttendeesListFragment extends BaseFragment implements AttendanceContract.TabItemView, AttendeesListContract.View {

    @BindView(R.id.tv_header_id)
    TextView tvHeaderId;
    @BindView(R.id.tv_attendance_count)
    TextView tvAttendanceCount;
    @BindView(R.id.recycler_view)
    RecyclerView rvAttendees;

    private AttendeesListAdapter mAdapter;
    private AttendeesListContract.Presenter mPresenter;

    public static AttendeesListFragment newInstance(EventActivityModel activityModel) {
        Bundle args = new Bundle();
        args.putParcelable(AppConstants.BUNDLE_ACTIVITY, activityModel);

        AttendeesListFragment fragment = new AttendeesListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_attendees_list, container, false);
        ButterKnife.bind(this, rootView);
        initialize();
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.REQUEST_SCAN_ATTENDANCE
                && resultCode == Activity.RESULT_OK
                && mPresenter != null){
            mPresenter.onScanAttendanceResult();
        }
    }

    private void initialize(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvAttendees.setLayoutManager(layoutManager);

        Bundle args = getArguments();
        if (args != null) {
            EventActivityModel activityModel = args.getParcelable(AppConstants.BUNDLE_ACTIVITY);

            new AttendeesListPresenterImpl(activityModel, this);

            if (mPresenter != null) {
                mAdapter = new AttendeesListAdapter(getActivity(), mPresenter);
                rvAttendees.setAdapter(mAdapter);
            }
        }
    }

    @OnClick(R.id.btn_scan)
    public void scanAttendees(){
        if (mPresenter != null) {
            mPresenter.onScanButtonClick();
        }
    }

    @Override
    public void setActivityModel(EventActivityModel activityModel) {
        if (mPresenter != null) {
            mPresenter.setActivityModel(activityModel);
        }
    }

    @Override
    public void setQuery(String query) {
        if (mPresenter != null) {
            mPresenter.onSearchQuery(query);
        }
    }

    @Override
    public void setPresenter(AttendeesListContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        if (getBaseActivity() instanceof AttendanceActivity) {
            ((AttendanceActivity) getBaseActivity()).onInvalidAuthToken(message);
        }
    }

    @Override
    public void setIdHeaderLabel(boolean isTeamActivity) {
        String label = isTeamActivity ? getString(R.string.label_id) : getString(R.string.label_sn);
        tvHeaderId.setText(label);
    }

    @Override
    public void setAttendanceCounter(int checkInCount, int totalCount) {
        String counterString = getString(R.string.format_attendance_number, checkInCount, totalCount);
        tvAttendanceCount.setText(counterString);
    }

    @Override
    public void updateAttendanceList() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void proceedToScanScreen(EventActivityModel activityModel) {
        Intent intent = new Intent(getActivity(), ScanAttendanceActivity.class);
        intent.putExtra(AppConstants.BUNDLE_ACTIVITY, activityModel);
        startActivityForResult(intent, AppConstants.REQUEST_SCAN_ATTENDANCE);
    }

    @Override
    public void requestActivityDataRefresh() {
        if (getBaseActivity() instanceof AttendanceContract.View) {
            ((AttendanceContract.View) getBaseActivity()).requestActivityDataRefresh();
        }
    }
}
