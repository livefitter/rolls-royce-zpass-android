package com.rolls_royce.events.zpass.base;

import android.support.annotation.Nullable;

/**
 * Created by LloydM on 10/19/17
 * for Livefitter
 */

public interface BaseServiceCallback {
    void onInvalidAuthenticationError(@Nullable String message);
}
