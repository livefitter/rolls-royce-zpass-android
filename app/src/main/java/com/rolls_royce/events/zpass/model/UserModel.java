package com.rolls_royce.events.zpass.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.rolls_royce.events.zpass.gson.UserPrefDeserializer;
import com.rolls_royce.events.zpass.gson.UserPrefSerializer;

import java.io.UnsupportedEncodingException;

/**
 * Created by LloydM on 9/15/17
 * for Livefitter
 */

public class UserModel implements Parcelable {

    private static String TAG = UserModel.class.getSimpleName();


    /*
          department: "Human Resources",
          id: 1,
          name: "Jisun Rombie",
          email: "jayson.livefitter@gmail.com",
          employee_id: "vwxyz-67890",
          gender: "Male",
          active: true,
          authentication_token: "some-token",
          sign_in_count: 1,
          qr_code_url: "some-url",
          house_id: 1,
          points: 100,
          role: "Employee",
          house: "House Awesome",
          house_image_url: "some-URL"
          tshirt_size: "XL"
     */

    @SerializedName("department_id")
    private int departmentId;
    private String department;
    private int id;
    private String name;
    private String email;
    @SerializedName("employee_id")
    private String employeeId;
    private String gender;
    @SerializedName("active")
    private boolean isActive;
    @SerializedName("authentication_token")
    private String authToken;
    @SerializedName("sign_in_count")
    private int signInCount;
    @SerializedName("qr_code_url")
    private String qrCodeUrl;
    @SerializedName("role_id")
    private int roleId;
    private String role;
    private int points;
    @SerializedName("house_id")
    private int houseId;
    @SerializedName("house")
    private String houseName;
    @SerializedName("house_image_url")
    private String houseImageUrl;
    @SerializedName("tshirt_size")
    private String shirtSize;

    public UserModel(int id, String email, String authToken) {
        this.id = id;
        this.email = email;
        this.authToken = authToken;
    }

    public UserModel(String name, String email, String authToken, boolean isActive) {
        this.name = name;
        this.email = email;
        this.authToken = authToken;
        this.isActive = isActive;
    }

    private UserModel(String name, String email, String employeeId) {
        this.name = name;
        this.email = email;
        this.employeeId = employeeId;
    }

    protected UserModel(Parcel in) {
        departmentId = in.readInt();
        department = in.readString();
        id = in.readInt();
        name = in.readString();
        email = in.readString();
        employeeId = in.readString();
        gender = in.readString();
        isActive = in.readByte() != 0;
        authToken = in.readString();
        signInCount = in.readInt();
        qrCodeUrl = in.readString();
        roleId = in.readInt();
        role = in.readString();
        points = in.readInt();
        houseId = in.readInt();
        houseName = in.readString();
        houseImageUrl = in.readString();
        shirtSize = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(departmentId);
        dest.writeString(department);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(employeeId);
        dest.writeString(gender);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeString(authToken);
        dest.writeInt(signInCount);
        dest.writeString(qrCodeUrl);
        dest.writeInt(roleId);
        dest.writeString(role);
        dest.writeInt(points);
        dest.writeInt(houseId);
        dest.writeString(houseName);
        dest.writeString(houseImageUrl);
        dest.writeString(shirtSize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };


    public int getDepartmentId() {
        return departmentId;
    }

    public String getDepartment() {
        return department;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getGender() {
        return gender;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getAuthToken() {
        return authToken;
    }

    public int getSignInCount() {
        return signInCount;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public int getRoleId() {
        return roleId;
    }

    public String getRole() {
        return role;
    }

    public int getPoints() {
        return points;
    }

    public int getHouseId() {
        return houseId;
    }

    public String getHouseName() {
        return houseName;
    }

    public String getHouseImageUrl() {
        return houseImageUrl;
    }

    public String getShirtSize() {
        return shirtSize;
    }

    public boolean matches(String filter) {
        return getName().contains(filter) || getEmail().contains(filter);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserModel) {
            UserModel other = (UserModel) obj;

            return this.getId() == other.getId();

            /*return this.getName().equals(other.getName())
                    && this.getEmail().equals(other.getEmail());*/

        }
        return false;
    }

    /**
     * Converts the given {@link UserModel} to a string representation of its name, email, authToken and isValid fields.
     * Uses {@link UserPrefSerializer} and {@link Gson} to convert only the relevant fields
     * and encodes it using {@link Base64#encodeToString(byte[], int)}
     *
     * @param userModel to convert to a string representation
     * @return an encoded Base64 string representation of this {@link UserModel}
     */
    public static String encode(UserModel userModel) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(UserModel.class, new UserPrefSerializer());

        String toEncode = builder.create().toJson(userModel, UserModel.class);

        try {
            byte[] bytes = toEncode.getBytes("UTF-8");
            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
            return encodedString;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Converts a given string to a {@link UserModel} containing its name, email, authToken and isValid fields.
     * Decodes it using {@link Base64#decode(String, int)}
     * and uses {@link UserPrefDeserializer} and {@link Gson} to retrieve only the relevant fields
     *
     * @param encodedUserModel String representation to decode
     * @return a valid {@link UserModel} or null if the decoding fails
     */
    public static UserModel decode(String encodedUserModel) {
        byte[] bytes = Base64.decode(encodedUserModel, Base64.DEFAULT);
        String decodedString = null;
        try {
            decodedString = new String(bytes, "UTF-8");

            if (!Strings.isNullOrEmpty(decodedString)) {

                GsonBuilder builder = new GsonBuilder();
                builder.registerTypeAdapter(UserModel.class, new UserPrefDeserializer());

                return builder.create().fromJson(decodedString, UserModel.class);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
