package com.rolls_royce.events.zpass.main.activity_list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.main.activity_list.ActivityListContract;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class ActivityItemViewHolder extends RecyclerView.ViewHolder implements ActivityListContract.RepositoryItemView {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_venue)
    TextView tvVenue;

    public static ActivityItemViewHolder create(LayoutInflater inflater, ViewGroup parent, View.OnClickListener clickListener){
        View view = inflater.inflate(R.layout.item_activity_list, parent, false);
        return new ActivityItemViewHolder(view, clickListener);
    }

    private ActivityItemViewHolder(View itemView, View.OnClickListener clickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(clickListener);
    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setDate(String date) {
        tvDate.setText(date);
    }

    @Override
    public void setVenue(String venue) {
        tvVenue.setText(venue);
    }
}
