package com.rolls_royce.events.zpass.main.event_list;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.EventModel;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public interface EventListContract {

    public interface Presenter extends BasePresenter {
        void retrieveEventList();
        int getEventCount();
        void bindItemView(RepositoryItemView itemView, int index);
        void onEventItemClick(int index);
    }

    public interface View extends BaseView<Presenter> {
        void updateEventList();
        void toggleUIEnabled(boolean isEnabled);
        void toggleProgressVisibility(boolean isVisible);
        void toggleSwipeProgress(boolean isVisible);
        void showRetrieveEventListError(String message);
        void proceedToActivityListScreen(int eventId);
    }

    public interface RepositoryItemView {
        void setName(String name);
        void setDate(String date);
    }
}
