package com.rolls_royce.events.zpass.main.attendees_list.presenter;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.rolls_royce.events.zpass.main.attendees_list.AttendeesListContract;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.ParticipantModel;

import java.util.ArrayList;

import javax.annotation.Nullable;

/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public class AttendeesListPresenterImpl implements AttendeesListContract.Presenter {

    private AttendeesListContract.View mView;
    private EventActivityModel mActivityModel;
    private ArrayList<ParticipantModel> mFilteredParticipants;

    public AttendeesListPresenterImpl(EventActivityModel activityModel, AttendeesListContract.View view) {
        this.mView = view;
        mActivityModel = activityModel;
        mFilteredParticipants = mActivityModel.getParticipants();
        mView.setPresenter(this);
    }

    private void prepareUI() {
        mView.setIdHeaderLabel(mActivityModel.isTeamActivity());

        int checkedInCount = mActivityModel.getTotalScanned() + mActivityModel.getTotalWalkIns();
        int totalCount = mActivityModel.getSlots() - mActivityModel.getAvailableSlots();
        mView.setAttendanceCounter(checkedInCount, totalCount);
    }

    @Override
    public void start() {
        //do nothing
    }

    @Override
    public void setActivityModel(EventActivityModel activityModel) {
        this.mActivityModel = activityModel;
        mFilteredParticipants = mActivityModel.getParticipants();
        if (mView.isActive()) {
            mView.updateAttendanceList();
            prepareUI();
        }
    }

    @Override
    public int getAttendanceCount() {
        return mActivityModel.getParticipants() != null ? mFilteredParticipants.size() : 0;
    }

    @Override
    public void bindItemView(AttendeesListContract.RepositoryItemView itemView, int index) {
        Preconditions.checkElementIndex(index, mFilteredParticipants.size());

        ParticipantModel participant = mFilteredParticipants.get(index);

        if (mActivityModel.isTeamActivity()) {
            itemView.setId(String.valueOf(participant.getTeamId()));
        }else{
            // uhhh.. it's supposed to be userId but they just really wanted the numerical sequence.
            //itemView.setId(String.valueOf(participant.getUserId()));
            // okay
            itemView.setId(String.valueOf(index + 1));
        }

        if (!Strings.isNullOrEmpty(participant.getEmail())) {
            itemView.setEmail(participant.getEmail(),
                    mActivityModel.isTeamActivity() && participant.isLeader());
        }

        if (!Strings.isNullOrEmpty(participant.getStatus())) {
            itemView.setStatus(participant.getStatus());
        }
    }

    @Override
    public void onScanButtonClick() {
        mView.proceedToScanScreen(mActivityModel);
    }

    @Override
    public void onSearchQuery(final String query) {
        if (Strings.isNullOrEmpty(query)) {
            //reset participants list
            mFilteredParticipants = mActivityModel.getParticipants();
        } else {
            /*mFilteredParticipants = new ArrayList<>();

            for (int i = 0; i < mActivityModel.getParticipants().size(); i++) {
                ParticipantModel participantModel = mActivityModel.getParticipants().get(i);
                if (participantModel.getEmail().contains(query)) {
                    mFilteredParticipants.add(participantModel);
                }
            }*/

            Iterable<ParticipantModel> participantIterable = Iterables.filter(mActivityModel.getParticipants(), new Predicate<ParticipantModel>() {
                @Override
                public boolean apply(@Nullable ParticipantModel input) {
                    return input != null && input.getEmail().toLowerCase().contains(query.toLowerCase());
                }
            });

            mFilteredParticipants = Lists.newArrayList(participantIterable);
        }

        mView.updateAttendanceList();
    }

    @Override
    public void onScanAttendanceResult() {
        mView.requestActivityDataRefresh();
    }
}