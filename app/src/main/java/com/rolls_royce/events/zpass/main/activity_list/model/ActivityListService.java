package com.rolls_royce.events.zpass.main.activity_list.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;
import com.rolls_royce.events.zpass.model.EventActivityModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public interface ActivityListService {
    void retrieveActivities(String authToken,
                            int eventId,
                            final RetrieveActivityListCallback callback);

    public interface RetrieveActivityListCallback extends BaseServiceCallback {

        void onRetrieveActivitySuccess(ArrayList<EventActivityModel> activityList);

        void onRetrieveActivityError(@Nullable String message);
    }
}