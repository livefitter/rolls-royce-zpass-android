package com.rolls_royce.events.zpass.data.service;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;


/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface LogOutService {
    void logOut(String authToken,
                final LogOutCallback callback);

    public interface LogOutCallback extends BaseServiceCallback {

        void onLogOutSuccess(String message);

        void onLogOutError(@Nullable String message);
    }
}