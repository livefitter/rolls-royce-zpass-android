package com.rolls_royce.events.zpass.base;

import android.support.v4.app.Fragment;


/**
 * Created by LloydM on 8/22/17
 * for Livefitter
 */

public class BaseFragment extends Fragment {

    public BaseActivity getBaseActivity(){
        if(getActivity() != null && getActivity() instanceof BaseActivity){
            return ((BaseActivity) getActivity());
        }

        return null;
    }

    /**
     * All activities implementing this class must call this method first when pressing the back button
     * @return true - if we allow the activity holding this class to handle normal back button behavior.
     * Set to true by default
     */
    public boolean onBackPressed(){
        return true;
    }

}
