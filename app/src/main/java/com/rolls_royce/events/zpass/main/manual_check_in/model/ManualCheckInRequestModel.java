package com.rolls_royce.events.zpass.main.manual_check_in.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public class ManualCheckInRequestModel {

    private Payload data;

    public ManualCheckInRequestModel(int activityId, String email){
        data = new Payload(activityId, email);
    }

    private class Payload{
        @SerializedName("activity_id")
        private int activityId;
        private ArrayList<PayloadMember> members;

        public Payload(int activityId, String email) {
            this.activityId = activityId;
            this.members = new ArrayList<PayloadMember>();
            members.add(new PayloadMember(email));
        }
    }

    private class PayloadMember {
        private String email;

        public PayloadMember(String email) {
            this.email = email;
        }
    }
}
