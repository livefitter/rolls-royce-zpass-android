package com.rolls_royce.events.zpass.main.log_in.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.BuildConfig;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.event_list.view.EventListActivity;
import com.rolls_royce.events.zpass.main.log_in.LogInContract;
import com.rolls_royce.events.zpass.main.log_in.presenter.LogInPresenterImpl;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.FormFieldTextWatcher;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogInActivity extends BaseActivity implements LogInContract.View {

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.progress_bar_layout)
    FrameLayout flProgressBarLayout;
    @BindView(R.id.btn_log_in)
    Button btnLogIn;
    @BindView(R.id.tv_version_number)
    TextView tvVersionNumber;

    LogInContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        String versionNumber = getString(R.string.format_version_number, BuildConfig.VERSION_NAME);

        tvVersionNumber.setText(versionNumber);

        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etPassword.addTextChangedListener(new FormFieldTextWatcher(tilPassword));

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_GO){
                    logIn();
                    return true;
                }
                return false;
            }
        });

        new LogInPresenterImpl(this);
    }

    @OnClick(R.id.btn_log_in)
    public void logIn() {
        if (mPresenter != null) {
            mPresenter.logInUser(etEmail.getText().toString(),
                    etPassword.getText().toString());
        }
    }

    @Override
    public void setPresenter(LogInContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        //do nothing
    }

    @Override
    public void clearPasswordField() {
        etPassword.getText().clear();
    }

    @Override
    public void showRequiredEmailError() {
        String fieldName = getString(R.string.label_email);
        String errorMessage = getString(R.string.error_required_field, fieldName);
        tilEmail.setError(errorMessage);
    }

    @Override
    public void showRequiredPasswordError() {
        String fieldName = getString(R.string.label_password);
        String errorMessage = getString(R.string.error_required_field, fieldName);
        tilPassword.setError(errorMessage);
    }

    @Override
    public void showInvalidEmailError() {
        String errorMessage = getString(R.string.error_invalid_email);
        tilEmail.setError(errorMessage);
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        flProgressBarLayout.setVisibility(visibility);
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {
        tilEmail.setEnabled(isEnabled);
        tilPassword.setEnabled(isEnabled);

        etEmail.setEnabled(isEnabled);
        etPassword.setEnabled(isEnabled);

        btnLogIn.setEnabled(isEnabled);
    }

    @Override
    public void saveUserData(UserModel userModel) {
        SharedPrefsUtil.saveCurrentUser(this, userModel);
    }

    @Override
    public void showLogInError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_log_in);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void proceedToEventsScreen() {
        Intent intent = new Intent(this, EventListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
