package com.rolls_royce.events.zpass.main.manual_registration.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.manual_registration.ManualRegistrationContract;
import com.rolls_royce.events.zpass.main.manual_registration.presenter.ManualRegistrationPresenterImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.FormFieldTextWatcher;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualRegistrationActivity extends BaseActivity implements ManualRegistrationContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_team_leader)
    TextView tvTeamLeader;
    @BindView(R.id.til_name)
    TextInputLayout tilName;
    @BindView(R.id.et_name)
    TextInputEditText etName;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.til_employee_id)
    TextInputLayout tilEmployeeId;
    @BindView(R.id.et_employee_id)
    TextInputEditText etEmployeeId;
    @BindView(R.id.layout_team_members)
    LinearLayout mTeamMembersLayout;
    @BindView(R.id.cb_indemnity)
    CheckBox cbIndemnity;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout mProgressBarLayout;

    private ManualRegistrationContract.Presenter mPresenter;
    private ArrayList<LinearLayout> mTeamMemberFieldViews;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_registration);
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_out) {
            showLogOutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        String title = getString(R.string.label_manual_registration).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        etName.addTextChangedListener(new FormFieldTextWatcher(tilName));
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etEmployeeId.addTextChangedListener(new FormFieldTextWatcher(tilEmployeeId));

        cbIndemnity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (mPresenter != null) {
                    mPresenter.onIndemnityCheckboxChecked(isChecked);
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.BUNDLE_ACTIVITY)) {
            UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
            EventActivityModel activityModel = extras.getParcelable(AppConstants.BUNDLE_ACTIVITY);

            if (userModel != null && activityModel != null) {
                new ManualRegistrationPresenterImpl(userModel, activityModel, this);
            }
        }

        if (mPresenter != null) {
            mPresenter.start();
        }
    }

    @OnClick(R.id.btn_confirm)
    public void confirmClick() {
        if (mPresenter != null) {
            mPresenter.confirmRegistration(convertLeaderFields(), convertMemberFields());
        }
    }

    private HashMap<String, String> convertLeaderFields() {
        HashMap<String, String> leaderMap = new HashMap<>();

        leaderMap.put(ManualRegistrationContract.KEY_NAME, etName.getText().toString());
        leaderMap.put(ManualRegistrationContract.KEY_EMAIL, etEmail.getText().toString());
        leaderMap.put(ManualRegistrationContract.KEY_EMPLOYEE_ID, etEmployeeId.getText().toString());

        return leaderMap;
    }

    private ArrayList<HashMap<String, String>> convertMemberFields() {

        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            ArrayList<HashMap<String, String>> memberMapList = new ArrayList<>();

            for (int i = 0; i < mTeamMemberFieldViews.size(); i++) {
                View teamMemberView = mTeamMemberFieldViews.get(i);
                HashMap<String, String> memberMap = null;
                if (teamMemberView != null) {
                    memberMap = new HashMap<>();

                    TextInputEditText etMemberName = teamMemberView.findViewById(R.id.et_name);
                    TextInputEditText etMemberEmail = teamMemberView.findViewById(R.id.et_email);
                    TextInputEditText etMemberEmployeeId = teamMemberView.findViewById(R.id.et_employee_id);

                    memberMap.put(ManualRegistrationContract.KEY_NAME, etMemberName.getText().toString());
                    memberMap.put(ManualRegistrationContract.KEY_EMAIL, etMemberEmail.getText().toString());
                    memberMap.put(ManualRegistrationContract.KEY_EMPLOYEE_ID, etMemberEmployeeId.getText().toString());
                }

                memberMapList.add(memberMap);
            }

            return memberMapList;
        }else {
            return null;
        }
    }

    @Override
    public void setPresenter(ManualRegistrationContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public void toggleTeamLeaderLabelVisibility(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        tvTeamLeader.setVisibility(visibility);
    }

    @Override
    public void toggleTeamMemberLayoutVisibility(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        mTeamMembersLayout.setVisibility(visibility);
    }

    @Override
    public void toggleConfirmButtonEnabled(boolean isEnabled) {
        btnConfirm.setEnabled(isEnabled);
    }

    @Override
    public void addTeamMemberEntryFields(int teamMemberCount) {
        mTeamMemberFieldViews = new ArrayList<>();

        for (int i = 0; i < teamMemberCount; i++) {
            LinearLayout teamMemberView = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_team_member, mTeamMembersLayout, false);

            TextInputLayout tilMemberName = teamMemberView.findViewById(R.id.til_name);
            TextInputEditText etMemberName = teamMemberView.findViewById(R.id.et_name);

            TextInputLayout tilMemberEmail = teamMemberView.findViewById(R.id.til_email);
            TextInputEditText etMemberEmail = teamMemberView.findViewById(R.id.et_email);

            TextInputLayout tilMemberEmployeeId = teamMemberView.findViewById(R.id.til_employee_id);
            TextInputEditText etMemberEmployeeId = teamMemberView.findViewById(R.id.et_employee_id);

            etMemberName.addTextChangedListener(new FormFieldTextWatcher(tilMemberName));
            etMemberEmail.addTextChangedListener(new FormFieldTextWatcher(tilMemberEmail));
            etMemberEmployeeId.addTextChangedListener(new FormFieldTextWatcher(tilMemberEmployeeId));

            mTeamMembersLayout.addView(teamMemberView);
            mTeamMemberFieldViews.add(teamMemberView);
        }
    }

    @Override
    public void showRequiredLeaderNameError() {
        String fieldName = getString(R.string.label_name);
        String message = getString(R.string.error_required_field, fieldName);
        tilName.setError(message);
    }

    @Override
    public void showRequiredLeaderEmailError() {
        String fieldName = getString(R.string.label_email);
        String message = getString(R.string.error_required_field, fieldName);
        tilEmail.setError(message);
    }

    @Override
    public void showRequiredLeaderEmployeIdError() {
        String fieldName = getString(R.string.label_employee_id);
        String message = getString(R.string.error_required_field, fieldName);
        tilEmployeeId.setError(message);
    }

    @Override
    public void showInvalidLeaderEmailError() {
        String message = getString(R.string.error_invalid_email);
        tilEmail.setError(message);
    }

    @Override
    public void showRequiredMemberNameError(int index) {
        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            Preconditions.checkElementIndex(index, mTeamMemberFieldViews.size());

            LinearLayout teamMemberView = mTeamMemberFieldViews.get(index);
            TextInputLayout tilMemberName = teamMemberView.findViewById(R.id.til_name);
            String fieldName = getString(R.string.label_name);
            String message = getString(R.string.error_required_field, fieldName);
            tilMemberName.setError(message);
        }
    }

    @Override
    public void showRequiredMemberEmailError(int index) {
        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            Preconditions.checkElementIndex(index, mTeamMemberFieldViews.size());

            LinearLayout teamMemberView = mTeamMemberFieldViews.get(index);
            TextInputLayout tilMemberEmail = teamMemberView.findViewById(R.id.til_email);
            String fieldName = getString(R.string.label_email);
            String message = getString(R.string.error_required_field, fieldName);
            tilMemberEmail.setError(message);
        }
    }

    @Override
    public void showRequiredMemberEmployeeIdError(int index) {
        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            Preconditions.checkElementIndex(index, mTeamMemberFieldViews.size());

            LinearLayout teamMemberView = mTeamMemberFieldViews.get(index);
            TextInputLayout tilMemberEmployeeId = teamMemberView.findViewById(R.id.til_employee_id);
            String fieldName = getString(R.string.label_employee_id);
            String message = getString(R.string.error_required_field, fieldName);
            tilMemberEmployeeId.setError(message);
        }
    }

    @Override
    public void showInvalidMemberEmailError(int index) {
        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            Preconditions.checkElementIndex(index, mTeamMemberFieldViews.size());

            LinearLayout teamMemberView = mTeamMemberFieldViews.get(index);
            TextInputLayout tilMemberEmail = teamMemberView.findViewById(R.id.til_email);
            String message = getString(R.string.error_invalid_email);
            tilMemberEmail.setError(message);
        }
    }

    @Override
    public void showRegistrationSuccess(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mPresenter != null) {
                    mPresenter.onSuccessDialogDismissed();
                }
            }
        });
    }

    @Override
    public void showRegistrationError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_register_activity);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {

        tilName.setEnabled(isEnabled);
        etName.setEnabled(isEnabled);
        tilEmail.setEnabled(isEnabled);
        etEmail.setEnabled(isEnabled);
        tilEmployeeId.setEnabled(isEnabled);
        etEmployeeId.setEnabled(isEnabled);

        //team member fields
        if (mTeamMemberFieldViews != null && !mTeamMemberFieldViews.isEmpty()) {
            for (int i = 0; i < mTeamMemberFieldViews.size(); i++) {
                LinearLayout teamMemberView = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_team_member, mTeamMembersLayout, false);

                TextInputLayout tilMemberName = teamMemberView.findViewById(R.id.til_name);
                TextInputEditText etMemberName = teamMemberView.findViewById(R.id.et_name);

                TextInputLayout tilMemberEmail = teamMemberView.findViewById(R.id.til_email);
                TextInputEditText etMemberEmail = teamMemberView.findViewById(R.id.et_email);

                TextInputLayout tilMemberEmployeeId = teamMemberView.findViewById(R.id.til_employee_id);
                TextInputEditText etMemberEmployeeId = teamMemberView.findViewById(R.id.et_employee_id);

                tilMemberName.setEnabled(isEnabled);
                etMemberName.setEnabled(isEnabled);
                tilMemberEmail.setEnabled(isEnabled);
                etMemberEmail.setEnabled(isEnabled);
                tilMemberEmployeeId.setEnabled(isEnabled);
                etMemberEmployeeId.setEnabled(isEnabled);
            }
        }

        cbIndemnity.setEnabled(isEnabled);
        btnConfirm.setEnabled(isEnabled);
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        mProgressBarLayout.setVisibility(visibility);
    }

    @Override
    public void proceedToScanningAttendanceActivity() {
        setResult(RESULT_OK);
        finish();
    }
}
