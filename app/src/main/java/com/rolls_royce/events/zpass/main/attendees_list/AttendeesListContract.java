package com.rolls_royce.events.zpass.main.attendees_list;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.EventActivityModel;

/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public interface AttendeesListContract {

    public interface Presenter extends BasePresenter {
        void setActivityModel(EventActivityModel activityModel);
        int getAttendanceCount();
        void bindItemView(RepositoryItemView itemView, int position);
        void onScanButtonClick();
        void onSearchQuery(String query);
        void onScanAttendanceResult();
    }

    public interface View extends BaseView<Presenter> {
        void setIdHeaderLabel(boolean isTeamActivity);
        void setAttendanceCounter(int checkInCount, int totalCount);
        void updateAttendanceList();
        void proceedToScanScreen(EventActivityModel activityModel);
        void requestActivityDataRefresh();
    }

    public interface RepositoryItemView{
        void setId(String id);
        void setEmail(String email, boolean isTeamLeader);
        void setStatus(String status);
    }
}
