package com.rolls_royce.events.zpass.main.manual_check_in.presenter;

import android.support.annotation.Nullable;
import android.util.Patterns;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.main.manual_check_in.ManualCheckInContract;
import com.rolls_royce.events.zpass.main.manual_check_in.model.ManualCheckInRequestModel;
import com.rolls_royce.events.zpass.main.manual_check_in.model.ManualCheckInService;
import com.rolls_royce.events.zpass.main.manual_check_in.model.ManualCheckInServiceImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;

/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public class ManualCheckInPresenterImpl implements ManualCheckInContract.Presenter, ManualCheckInService.Callback {

    private ManualCheckInContract.View mView;
    private UserModel mUserModel;
    private EventActivityModel mActivityModel;
    private ManualCheckInService mService;

    public ManualCheckInPresenterImpl(UserModel userModel, EventActivityModel activityModel, ManualCheckInContract.View view) {
        this.mView = view;
        mUserModel = userModel;
        mActivityModel = activityModel;
        mService = new ManualCheckInServiceImpl();
        mView.setPresenter(this);
    }

    private void prepareUI() {
        // If the indemnity checkbox is mandatory by virtue of being a team activity
        // or it's an individual activity
        // disable the confirmation button
        if ((mActivityModel.isTeamActivity() && mActivityModel.isSurveyMandatory())
                || !mActivityModel.isTeamActivity()) {
            mView.toggleCheckInButtonEnabled(false);
        }
    }

    private boolean validateFields(String email){
        boolean areFieldsValid = true;

        if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(email.trim())) {
            areFieldsValid = false;
            mView.showRequiredEmailError();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            areFieldsValid = false;
            mView.showInvalidEmailError();
        }

        return areFieldsValid;
    }

    @Override
    public void start() {
        prepareUI();
    }

    @Override
    public void onIndemnityCheckboxChecked(boolean isChecked) {
        if ((mActivityModel.isTeamActivity() && mActivityModel.isSurveyMandatory())
                || !mActivityModel.isTeamActivity()) {
            mView.toggleCheckInButtonEnabled(isChecked);
        }
    }

    @Override
    public void checkInUser(String email) {
        if (validateFields(email)) {
            mView.toggleUIEnabled(false);
            mView.toggleProgressVisibility(true);

            mService.checkInUser(mUserModel.getAuthToken(),
                    new ManualCheckInRequestModel(mActivityModel.getId(), email),
                    this);
        }
    }

    @Override
    public void onSuccessDialogDismissed() {
        mView.proceedToScanningAttendanceActivity();
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onCheckInSuccess(String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleUIEnabled(true);
            mView.toggleProgressVisibility(false);

            mView.showCheckInSuccess(message);
        }
    }

    @Override
    public void onCheckInError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleUIEnabled(true);
            mView.toggleProgressVisibility(false);

            mView.showCheckInError(message);
        }
    }
}