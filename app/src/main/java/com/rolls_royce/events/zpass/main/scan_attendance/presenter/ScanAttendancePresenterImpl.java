package com.rolls_royce.events.zpass.main.scan_attendance.presenter;

import android.os.Handler;
import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.main.scan_attendance.ScanAttendanceContract;
import com.rolls_royce.events.zpass.main.scan_attendance.model.ScanAttendanceRequestModel;
import com.rolls_royce.events.zpass.main.scan_attendance.model.ScanAttendanceService;
import com.rolls_royce.events.zpass.main.scan_attendance.model.ScanAttendanceServiceImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;

/**
 * Created by LloydM on 11/20/17
 * for Livefitter
 */

public class ScanAttendancePresenterImpl implements ScanAttendanceContract.Presenter, ScanAttendanceService.validateQrCodeCallback {

    private ScanAttendanceContract.View mView;
    private UserModel mUserModel;
    private EventActivityModel mActivityModel;
    private ScanAttendanceService mService;

    private final Handler mHandler = new Handler();
    private int mTotalScanCount;
    private boolean hasSuccessfullyTransacted;

    public ScanAttendancePresenterImpl(UserModel userModel, EventActivityModel activityModel, ScanAttendanceContract.View view) {
        this.mView = view;
        mUserModel = userModel;
        mActivityModel = activityModel;
        mService = new ScanAttendanceServiceImpl();
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.toggleProgressVisibility(false);
        mView.setTotalScanCount(mTotalScanCount);
        prepareScan();
    }

    @Override
    public void prepareScan() {
        if (mView.mayUseCamera()) {
            mView.toggleEnableScanning(true, false);
            mView.setScanStatus(AppConstants.SCAN_STATUS_READY);
        } else {
            disableScan();
        }
    }

    @Override
    public void resumeScan() {
        mView.toggleEnableScanning(true, true);
        mView.setScanStatus(AppConstants.SCAN_STATUS_READY);
    }

    @Override
    public void disableScan() {
        mView.toggleEnableScanning(false, false);
        mView.setScanStatus(AppConstants.SCAN_STATUS_DISABLED);
    }

    @Override
    public void onRegistrationClick() {
        mView.proceedToRegistrationScreen(mActivityModel);
    }

    @Override
    public void onCheckInClick() {
        mView.proceedToCheckInScreen(mActivityModel);
    }

    @Override
    public void onQRScanSuccess(String qrContent) {
        mView.setScanStatus(AppConstants.SCAN_STATUS_VALIDATING);
        mView.togglePauseScanning(true);
        mView.toggleProgressVisibility(true);
        mView.toggleUIEnabled(false);

        boolean isHandled = false;
        int userId = -1;

        try {
            if (!Strings.isNullOrEmpty(qrContent)) {
                isHandled = true;
                userId = Integer.decode(qrContent);
            }

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } finally {
            if (isHandled) {
                mService.validateQrCode(mUserModel.getAuthToken(),
                        new ScanAttendanceRequestModel(userId, mActivityModel.getId()),
                        this);
            } else {
                onQRScanFailed();
            }
        }
    }

    @Override
    public void onQRScanFailed() {
        mView.setScanStatus(AppConstants.SCAN_STATUS_FAILED);
        mView.togglePauseScanning(true);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.setScanStatus(AppConstants.SCAN_STATUS_READY);
                mView.togglePauseScanning(false);
            }
        }, 1000);
    }

    @Override
    public void onTransactionRequestSuccess() {
        hasSuccessfullyTransacted = true;
    }

    @Override
    public void onDialogMessageDismissed() {
        mView.setScanStatus(AppConstants.SCAN_STATUS_READY);
        mView.togglePauseScanning(false);
    }

    @Override
    public boolean hasSuccessfullyTransacted() {
        return hasSuccessfullyTransacted;
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onValidateQrCodeSuccess(String message) {
        mTotalScanCount++;
        hasSuccessfullyTransacted = true;

        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleUIEnabled(true);
            mView.setTotalScanCount(mTotalScanCount);
            mView.showValidQrCodeDialog(message);
        }

    }

    @Override
    public void onValidateQrCodeError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleUIEnabled(true);
            mView.showInvalidQrCodeError(message);
        }
    }
}