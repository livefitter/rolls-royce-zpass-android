package com.rolls_royce.events.zpass.main.manual_registration.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.rolls_royce.events.zpass.model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualTeamRegistrationRequestModel {

    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_EMPLOYEE_ID = "employee_id";

    private Payload data;

    public ManualTeamRegistrationRequestModel(int activityId, ArrayList<HashMap<String, String>> userList) {
        List<PayloadMember> members = new ArrayList<>();

        for (int i = 0; i < userList.size(); i++) {
            HashMap<String, String> userMap = userList.get(i);
            members.add(new PayloadMember(userMap.get(KEY_NAME),
                    userMap.get(KEY_EMPLOYEE_ID),
                    userMap.get(KEY_EMAIL)));
        }

        data = new Payload(activityId, members);
    }

    private class Payload {

        @SerializedName("activity_id")
        private int activityId;
        private List<PayloadMember> members;

        public Payload(int activityId, List<PayloadMember> members) {
            this.activityId = activityId;
            this.members = members;
        }
    }

    private class PayloadMember {

        private String name;
        @SerializedName("employee_id")
        private String employeeId;
        private String email;

        public PayloadMember(String name, String employeeId, String email) {
            this.name = name;
            this.employeeId = employeeId;
            this.email = email;
        }
    }
}
