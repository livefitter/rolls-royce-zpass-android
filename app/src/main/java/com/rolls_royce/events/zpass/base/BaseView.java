package com.rolls_royce.events.zpass.base;

/**
 * Created by LloydM on 8/18/17
 * for Livefitter
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

    boolean isActive();

    void onInvalidAuthToken(String message);
}
