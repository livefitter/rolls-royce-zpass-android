package com.rolls_royce.events.zpass.main.scan_attendance.view;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.manual_check_in.view.ManualCheckInActivity;
import com.rolls_royce.events.zpass.main.manual_registration.view.ManualRegistrationActivity;
import com.rolls_royce.events.zpass.main.scan_attendance.ScanAttendanceContract;
import com.rolls_royce.events.zpass.main.scan_attendance.presenter.ScanAttendancePresenterImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;


/**
 * Created by LloydM on 11/20/17
 * for Livefitter
 */

public class ScanAttendanceActivity extends BaseActivity implements ScanAttendanceContract.View {

    /**
     * Id to identity CAMERA permission mRequest.
     */
    private static final int REQUEST_CAMERA = 1029;

    @BindView(R.id.root_layout)
    CoordinatorLayout mLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.barcode_view)
    DecoratedBarcodeView mBarcodeView;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout mProgressBarLayout;
    @BindView(R.id.tv_scan_count)
    TextView tvScanCount;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.btn_check_in)
    Button btnCheckIn;

    private ScanAttendanceContract.Presenter mPresenter;
    private boolean HAS_PROMPTED_CAMERA_RATIONALE;

    private BarcodeCallback mBarcodeCallback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            boolean isScanSuccessful = result != null && !Strings.isNullOrEmpty(result.getText());

            if (mPresenter != null) {
                if (isScanSuccessful) {
                    LoggingHelper.debug(this, "barcodeResult", "result: " + result.getText() + "---End");
                    mPresenter.onQRScanSuccess(result.getText());
                } else {
                    mPresenter.onQRScanFailed();
                }
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
            //do nothing
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_attendance);
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPresenter != null) {
            mPresenter.resumeScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mPresenter != null) {
            mPresenter.disableScan();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mPresenter != null) {
            mPresenter.disableScan();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // Return to AttendanceScreen and refresh activity data
            if (mPresenter != null && mPresenter.hasSuccessfullyTransacted()) {
                setResult(RESULT_OK);
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Return to AttendanceScreen and refresh activity data
        if (mPresenter != null && mPresenter.hasSuccessfullyTransacted()) {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == AppConstants.REQUEST_MANUAL_REGISTRATION
                || requestCode == AppConstants.REQUEST_MANUAL_CHECK_IN)
                && resultCode == RESULT_OK
                && mPresenter != null) {
            mPresenter.onTransactionRequestSuccess();
        }
    }

    private void initialize() {
        String title = getString(R.string.label_scan_attendees).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.BUNDLE_ACTIVITY)) {
            UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
            EventActivityModel activityModel = extras.getParcelable(AppConstants.BUNDLE_ACTIVITY);

            if (userModel != null && activityModel != null) {

                new ScanAttendancePresenterImpl(userModel, activityModel, this);
            }
        }

        if (mPresenter != null) {
            mPresenter.start();
        }
    }

    @OnClick(R.id.btn_register)
    public void registerButtonClick() {
        if (mPresenter != null) {
            mPresenter.onRegistrationClick();
        }
    }

    @OnClick(R.id.btn_check_in)
    public void checkInButtonClick() {
        if (mPresenter != null) {
            mPresenter.onCheckInClick();
        }
    }

    @Override
    public void setPresenter(ScanAttendanceContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public boolean mayUseCamera() {
        // No need to request for permissions for android versions earlier than Marshmallow
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {
            // For Marshmallow onwards
            if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            if (shouldShowRequestPermissionRationale(CAMERA)) {
                Snackbar.make(mLayout, R.string.permission_rationale_camera, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                            }
                        })
                        .show();
            } else {
                requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
            }
        }
        return false;
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        if (mProgressBarLayout != null) {
            int visibility = isVisible ? View.VISIBLE : View.GONE;
            mProgressBarLayout.setVisibility(visibility);
        }
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {
        btnRegister.setEnabled(isEnabled);
        btnCheckIn.setEnabled(isEnabled);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA && mPresenter != null) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mPresenter.prepareScan();
            } else if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                mPresenter.disableScan();

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA)) {
                    // Show the permissions prompt as a expanded reason as to why
                    // this app needs camera permissions
                    // but only show it once to the user
                    if (!HAS_PROMPTED_CAMERA_RATIONALE) {
                        HAS_PROMPTED_CAMERA_RATIONALE = true;
                        String title_res = getString(R.string.permission_prompt_camera_title);
                        String message_res = getString(R.string.permission_prompt_camera_message);
                        AppCompatDialog alert = createPermissionsPrompt(title_res, message_res);
                        alert.show();
                    }
                } else {
                    // User has been informed on the rationale but still denied permission
                    // Display a snackbar as a last ditch prompt to the user
                    Snackbar.make(mLayout, getString(R.string.permission_settings_prompt_camera), Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.label_settings), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // Show the app settings for this app to the user so they can change the permissions themselves.
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            }).show();
                }
            }
        }
    }

    /**
     * Creates a dialog that prompts the user to reconsider granting the app permission to use the camera
     *
     * @param title   String resource for the title
     * @param message String resource for the message
     * @return The alert dialog to show to the user
     */
    private AppCompatDialog createPermissionsPrompt(String title, String message) {
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(getString(R.string.label_retry), new DialogInterface.OnClickListener() {
                    @Override
                    @TargetApi(Build.VERSION_CODES.M)
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                    }
                })
                .setPositiveButton(getString(R.string.label_sure), null)
                .create();
    }

    @Override
    public void toggleEnableScanning(boolean enableScan, boolean resumeOngoingScan) {
        if (enableScan) {
            if (resumeOngoingScan) {
                mBarcodeView.resume();
            }else {
                mBarcodeView.decodeContinuous(mBarcodeCallback);
            }
        } else {
            mBarcodeView.pause();
        }
    }

    @Override
    public void togglePauseScanning(boolean pauseScan) {
        if (pauseScan) {
            mBarcodeView.getBarcodeView().stopDecoding();
        } else {
            mBarcodeView.decodeContinuous(mBarcodeCallback);
        }
    }

    @Override
    public void setScanStatus(int scanStatus) {
        String statusMessage = "";
        switch (scanStatus) {
            case AppConstants.SCAN_STATUS_DISABLED:
                statusMessage = getString(R.string.label_scan_disabled);
                break;
            case AppConstants.SCAN_STATUS_READY:
                statusMessage = getString(R.string.label_scan_ready);
                break;
            case AppConstants.SCAN_STATUS_FAILED:
                statusMessage = getString(R.string.label_scan_failed);
                break;
            case AppConstants.SCAN_STATUS_VALIDATING:
                statusMessage = getString(R.string.label_scan_validating);
                break;
        }

        if (!Strings.isNullOrEmpty(statusMessage)) {
            mBarcodeView.setStatusText(statusMessage);
        }
    }

    @Override
    public void showValidQrCodeDialog(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mPresenter != null) {
                    mPresenter.onDialogMessageDismissed();
                }
            }
        });
    }

    @Override
    public void showInvalidQrCodeError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_valdiate_qr_code);
        }
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mPresenter != null) {
                    mPresenter.onDialogMessageDismissed();
                }
            }
        });
    }

    @Override
    public void setTotalScanCount(int totalScanCount) {
        String message = getString(R.string.format_total_scanned, totalScanCount);
        tvScanCount.setText(message);
    }

    @Override
    public void proceedToRegistrationScreen(EventActivityModel activityModel) {
        Intent intent = new Intent(this, ManualRegistrationActivity.class);
        intent.putExtra(AppConstants.BUNDLE_ACTIVITY, activityModel);
        startActivityForResult(intent, AppConstants.REQUEST_MANUAL_REGISTRATION);
    }

    @Override
    public void proceedToCheckInScreen(EventActivityModel activityModel) {
        Intent intent = new Intent(this, ManualCheckInActivity.class);
        intent.putExtra(AppConstants.BUNDLE_ACTIVITY, activityModel);
        startActivityForResult(intent, AppConstants.REQUEST_MANUAL_CHECK_IN);
    }
}
