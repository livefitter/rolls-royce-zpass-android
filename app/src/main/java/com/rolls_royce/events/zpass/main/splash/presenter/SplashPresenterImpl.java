package com.rolls_royce.events.zpass.main.splash.presenter;

/**
 * Created by LloydM on 10/2/17
 * for Livefitter
 */

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.main.splash.SplashContract;
import com.rolls_royce.events.zpass.model.UserModel;


public class SplashPresenterImpl implements SplashContract.Presenter {

    private SplashContract.View mView;
    private UserModel mUserModel;

    public SplashPresenterImpl(@Nullable UserModel userModel, SplashContract.View view) {
        this.mView = view;
        this.mUserModel = userModel;
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        checkUserLogIn();
    }

    private void checkUserLogIn() {
        if (mUserModel != null) {
            mView.proceedToEventsScreen();
        } else {
            mView.proceedToLogInScreen();
        }
    }
}