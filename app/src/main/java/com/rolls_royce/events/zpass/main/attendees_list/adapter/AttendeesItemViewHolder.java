package com.rolls_royce.events.zpass.main.attendees_list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.main.attendees_list.AttendeesListContract;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public class AttendeesItemViewHolder extends RecyclerView.ViewHolder implements AttendeesListContract.RepositoryItemView {

    View mRootView;
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_status)
    TextView tvStatus;

    public static AttendeesItemViewHolder create(LayoutInflater inflater, ViewGroup parent){
        View rootView = inflater.inflate(R.layout.item_attendees, parent, false);
        return new AttendeesItemViewHolder(rootView);
    }

    private AttendeesItemViewHolder(View view){
        super(view);
        ButterKnife.bind(this, view);
        mRootView = view;
    }

    @Override
    public void setId(String id) {
        tvId.setText(id);
    }

    @Override
    public void setEmail(String email, boolean isTeamLeader) {
        if (isTeamLeader) {
            email = mRootView.getContext().getString(R.string.format_attendance_leader, email);
        }
        tvEmail.setText(email);
    }

    @Override
    public void setStatus(String status) {
        tvStatus.setText(status);
    }
}
