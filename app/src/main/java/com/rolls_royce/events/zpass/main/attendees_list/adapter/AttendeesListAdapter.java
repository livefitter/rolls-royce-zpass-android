package com.rolls_royce.events.zpass.main.attendees_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rolls_royce.events.zpass.main.attendees_list.AttendeesListContract;


/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public class AttendeesListAdapter extends RecyclerView.Adapter<AttendeesItemViewHolder> {

    private LayoutInflater mLayoutInflater;
    private AttendeesListContract.Presenter mPresenter;

    public AttendeesListAdapter(Context context, AttendeesListContract.Presenter presenter){
        mLayoutInflater = LayoutInflater.from(context);
        mPresenter = presenter;
    }

    @Override
    public AttendeesItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return AttendeesItemViewHolder.create(mLayoutInflater, parent) ;
    }

    @Override
    public void onBindViewHolder(AttendeesItemViewHolder holder, int position) {
        if (mPresenter != null) {
            mPresenter.bindItemView(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mPresenter != null? mPresenter.getAttendanceCount() : 0;
    }
}
