package com.rolls_royce.events.zpass.main.manual_check_in.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualCheckInServiceImpl implements ManualCheckInService {
    private ManualCheckInApiMethods mApiMethods;

    private void initialize() {
        if (mApiMethods == null) {
            mApiMethods = RetrofitUtil.getGSONRetrofit().create(ManualCheckInApiMethods.class);
        }
    }

    @Override
    public void checkInUser(String authToken, ManualCheckInRequestModel body, final Callback callback) {
        initialize();

        mApiMethods.checkInUser(authToken, body).enqueue(new retrofit2.Callback<BaseResponseModel>() {
            @Override
            public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true message: " + response.body().getMessage());
                        callback.onCheckInSuccess(response.body().getMessage());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onCheckInError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onCheckInError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onCheckInError(null);
            }
        });
    }
}
