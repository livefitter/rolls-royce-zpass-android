package com.rolls_royce.events.zpass.main.attendance.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.google.common.base.Preconditions;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseFragment;
import com.rolls_royce.events.zpass.main.attendance.AttendanceContract;
import com.rolls_royce.events.zpass.main.attendees_list.view.AttendeesListFragment;
import com.rolls_royce.events.zpass.main.ranking.view.RankingFragment;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 7/12/2017.
 */

public class AttendancePagerAdapter extends FragmentPagerAdapter {

    private final AttendanceContract.Presenter mPresenter;
    private ArrayList<BaseFragment> mFragmentList;
    private String titleAttendees, titleRanking;

    public AttendancePagerAdapter(Context context, FragmentManager fm, AttendanceContract.Presenter presenter) {
        super(fm);
        this.mPresenter = presenter;

        if(mPresenter.getActivityModel() != null) {
            mFragmentList = new ArrayList<>();
            mFragmentList.add(AttendeesListFragment.newInstance(mPresenter.getActivityModel()));
            mFragmentList.add(RankingFragment.newInstance(mPresenter.getActivityModel()));

            titleAttendees = context.getString(R.string.label_attendees);
            titleRanking = context.getString(R.string.label_ranking);
        }
    }

    @Override
    public Fragment getItem(int position) {
        Preconditions.checkElementIndex(position, mFragmentList.size());
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList != null ? mFragmentList.size() : 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return titleAttendees;
            case 1:
                return titleRanking;
            default:
                return null;
        }
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            //Weird fix for a bug that is described here:
            // https://stackoverflow.com/questions/41650721/attempt-to-invoke-virtual-method-android-os-handler-android-support-v4-app-frag
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }

    public void setActivityModelToChild(EventActivityModel activityModel){
        LoggingHelper.debug(this, "setActivityModelToChild", "activityModel: " +activityModel);
        for (int i = 0; i < mFragmentList.size(); i++) {
            BaseFragment fragment = mFragmentList.get(i);
            if (fragment instanceof AttendanceContract.TabItemView) {
                ((AttendanceContract.TabItemView) fragment).setActivityModel(activityModel);
            }
        }
    }

    public void setQueryToChild(int index, String query){
        Preconditions.checkElementIndex(index, mFragmentList.size());

        BaseFragment fragment = mFragmentList.get(index);
        if (fragment instanceof AttendanceContract.TabItemView) {
            ((AttendanceContract.TabItemView) fragment).setQuery(query);
        }
    }
}
