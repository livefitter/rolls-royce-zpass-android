package com.rolls_royce.events.zpass.main.attendance.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.data.EventActivitiesApiMethods;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public class RetrieveActivityDetailsServiceImpl implements RetrieveActivityDetailsService {

    private EventActivitiesApiMethods mActivitiesApiMethods;

    private void initialize() {
        if(mActivitiesApiMethods == null) {
            mActivitiesApiMethods = RetrofitUtil.getGSONRetrofit().create(EventActivitiesApiMethods.class);
        }
    }

    @Override
    public void retrieveActivityDetail(String authToken, int activityId, final Callback callback) {
        initialize();

        mActivitiesApiMethods.retrieveActivity(authToken, activityId).enqueue(new retrofit2.Callback<BaseResponseModel<EventActivityModel>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<EventActivityModel>> call, Response<BaseResponseModel<EventActivityModel>> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true data: " + response.body().getData());
                        callback.onSuccess(response.body().getData());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<EventActivityModel>> call, Throwable t) {
                t.printStackTrace();
                callback.onError(null);
            }
        });
    }
}
