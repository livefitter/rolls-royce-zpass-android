package com.rolls_royce.events.zpass.main.ranking.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseFragment;
import com.rolls_royce.events.zpass.model.EventActivityModel;

import butterknife.ButterKnife;


/**
 * Created by LloydM on 11/20/17
 * for Livefitter
 */

public class RankingFragment extends BaseFragment {

    public static RankingFragment newInstance(EventActivityModel activityModel) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstants.BUNDLE_ACTIVITY, activityModel);

        RankingFragment fragment = new RankingFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ranking, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }
}
