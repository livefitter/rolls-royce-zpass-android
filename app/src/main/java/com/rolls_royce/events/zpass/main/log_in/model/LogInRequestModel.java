package com.rolls_royce.events.zpass.main.log_in.model;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class LogInRequestModel {

    private Payload data;

    public LogInRequestModel(String email, String password){
        this.data = new Payload(email, password);
    }

    private class Payload{
        private String email;
        private String password;

        public Payload(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}
