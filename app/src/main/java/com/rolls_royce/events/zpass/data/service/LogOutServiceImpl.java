package com.rolls_royce.events.zpass.data.service;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.data.LogOutApiMethods;
import com.rolls_royce.events.zpass.main.log_in.model.LogInApiMethods;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class LogOutServiceImpl implements LogOutService {
    @Override
    public void logOut(String authToken, final LogOutCallback callback) {
        LogOutApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(LogOutApiMethods.class);

        apiMethods.logOutUser(authToken).enqueue(new Callback<BaseResponseModel>() {
            @Override
            public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true message: " + response.body().getMessage());
                        callback.onLogOutSuccess(response.body().getMessage());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onLogOutError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onLogOutError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onLogOutError(null);
            }
        });
    }
}
