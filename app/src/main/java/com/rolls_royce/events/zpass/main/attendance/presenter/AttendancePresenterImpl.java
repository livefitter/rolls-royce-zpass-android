package com.rolls_royce.events.zpass.main.attendance.presenter;

import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.rolls_royce.events.zpass.main.attendance.AttendanceContract;
import com.rolls_royce.events.zpass.main.attendance.model.RetrieveActivityDetailsService;
import com.rolls_royce.events.zpass.main.attendance.model.RetrieveActivityDetailsServiceImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;

/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public class AttendancePresenterImpl implements AttendanceContract.Presenter, RetrieveActivityDetailsService.Callback {

    private final UserModel mUserModel;
    private EventActivityModel mActivityModel;
    private AttendanceContract.View mView;
    private RetrieveActivityDetailsService mService;

    public AttendancePresenterImpl(UserModel userModel, EventActivityModel activityModel, AttendanceContract.View view) {
        this.mView = view;
        this.mUserModel = userModel;
        this.mActivityModel = activityModel;
        this.mService = new RetrieveActivityDetailsServiceImpl();
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        mView.toggleProgressVisibility(true);
        retrieveActivityDetails();
    }

    @Override
    public EventActivityModel getActivityModel() {
        return mActivityModel;
    }

    @Override
    public void retrieveActivityDetails() {
        mService.retrieveActivityDetail(mUserModel.getAuthToken(),
                mActivityModel.getId(),
                this);
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onSuccess(EventActivityModel activityModel) {
        mActivityModel = activityModel;

        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.setActivityModel(mActivityModel);
            mView.toggleProgressVisibility(false);
        }
    }

    @Override
    public void onError(@Nullable String message) {

        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.showActivityRetrievalError(message);
        }
    }
}