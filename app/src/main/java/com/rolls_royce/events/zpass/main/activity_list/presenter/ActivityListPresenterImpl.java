package com.rolls_royce.events.zpass.main.activity_list.presenter;

import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.main.activity_list.ActivityListContract;
import com.rolls_royce.events.zpass.main.activity_list.model.ActivityListService;
import com.rolls_royce.events.zpass.main.activity_list.model.ActivityListServiceImpl;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;

import java.util.ArrayList;

/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class ActivityListPresenterImpl implements ActivityListContract.Presenter, ActivityListService.RetrieveActivityListCallback {

    private ActivityListContract.View mView;
    private ActivityListService mService;
    private final UserModel mUserModel;
    private final int mEventId;
    private ArrayList<EventActivityModel> mActivityList;

    private boolean isInitialLoad = true;

    public ActivityListPresenterImpl(UserModel userModel, int eventId, ActivityListContract.View view) {
        this.mUserModel = userModel;
        this.mEventId = eventId;
        this.mView = view;
        this.mService = new ActivityListServiceImpl();
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        retrieveActivityList();
    }

    @Override
    public void retrieveActivityList() {
        if (isInitialLoad) {
            mView.toggleProgressVisibility(true);
        }

        mService.retrieveActivities(mUserModel.getAuthToken(), mEventId, this);
    }

    @Override
    public int getActivityCount() {
        return mActivityList != null ? mActivityList.size() : 0;
    }

    @Override
    public void bindItemView(ActivityListContract.RepositoryItemView itemView, int index) {
        Preconditions.checkElementIndex(index, mActivityList.size());
        EventActivityModel activityModel = mActivityList.get(index);

        if (!Strings.isNullOrEmpty(activityModel.getName())) {
            itemView.setName(activityModel.getName());
        }

        if (!Strings.isNullOrEmpty(activityModel.getDateTime())) {
            itemView.setDate(activityModel.getDateTime());
        }

        if (!Strings.isNullOrEmpty(activityModel.getVenue())) {
            itemView.setVenue(activityModel.getVenue());
        }
    }

    @Override
    public void onActivityItemClick(int index) {
        Preconditions.checkElementIndex(index, mActivityList.size());
        EventActivityModel activityModel = mActivityList.get(index);

        mView.proceedToAttendanceScreen(activityModel);
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onRetrieveActivitySuccess(ArrayList<EventActivityModel> activityList) {
        mActivityList = activityList;
        isInitialLoad = false;

        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleSwipeProgress(false);

            mView.toggleUIEnabled(!mActivityList.isEmpty());
            mView.updateActivityList();
        }
    }

    @Override
    public void onRetrieveActivityError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleSwipeProgress(false);
            mView.showRetrieveActivityListError(message);
        }
    }
}