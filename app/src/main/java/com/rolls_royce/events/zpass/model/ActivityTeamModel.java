package com.rolls_royce.events.zpass.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public class ActivityTeamModel implements Parcelable{


    /**
     * rank : 1st
     * team_id : 10555
     * team_leader_email : leader_1@gmail.com
     */


    @SerializedName("team_id")
    private int teamId;
    @SerializedName("team_leader_email")
    private String leaderEmail;
    private String rank;

    protected ActivityTeamModel(Parcel in) {
        teamId = in.readInt();
        leaderEmail = in.readString();
        rank = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(teamId);
        dest.writeString(leaderEmail);
        dest.writeString(rank);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActivityTeamModel> CREATOR = new Creator<ActivityTeamModel>() {
        @Override
        public ActivityTeamModel createFromParcel(Parcel in) {
            return new ActivityTeamModel(in);
        }

        @Override
        public ActivityTeamModel[] newArray(int size) {
            return new ActivityTeamModel[size];
        }
    };

    public int getTeamId() {
        return teamId;
    }

    public String getLeaderEmail() {
        return leaderEmail;
    }

    public String getRank() {
        return rank;
    }
}
