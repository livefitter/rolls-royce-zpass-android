package com.rolls_royce.events.zpass.main.manual_check_in;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public interface ManualCheckInContract {

    public interface Presenter extends BasePresenter {
        void onIndemnityCheckboxChecked(boolean isChecked);

        void checkInUser(String email);

        void onSuccessDialogDismissed();
    }

    public interface View extends BaseView<Presenter> {

        void toggleCheckInButtonEnabled(boolean isEnabled);

        void showRequiredEmailError();

        void showInvalidEmailError();

        void showCheckInSuccess(String message);

        void showCheckInError(String message);

        void toggleUIEnabled(boolean isEnabled);

        void toggleProgressVisibility(boolean isVisible);

        void proceedToScanningAttendanceActivity();
    }
}
