package com.rolls_royce.events.zpass.main.ranking;

import android.view.View;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;

/**
 * Created by LloydM on 11/20/17
 * for Livefitter
 */

public interface RankingContract {

    public interface Presenter extends BasePresenter {
        int getTeamCount();
        void bindItemView(RepositoryItemView itemView, int index);
        void onSearchQuery(String query);
        void awardRanking(int selectedTeamIndex);
        void onScanAttendanceResult();
    }

    public interface View extends BaseView<Presenter> {
        void updateTeamList();
        void requestActivityDataRefresh();
    }

    public interface RepositoryItemView {
        android.view.View getRootView();
        void setId(String id);
        void setLeaderEmail(String leaderEmail);
        void setRank(int rank);
    }
}
