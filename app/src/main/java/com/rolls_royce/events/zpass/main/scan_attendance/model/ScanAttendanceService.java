package com.rolls_royce.events.zpass.main.scan_attendance.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;


/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface ScanAttendanceService {
    void validateQrCode(String authToken,
                        ScanAttendanceRequestModel body,
                        final validateQrCodeCallback callback);

    public interface validateQrCodeCallback extends BaseServiceCallback {

        void onValidateQrCodeSuccess(String message);

        void onValidateQrCodeError(@Nullable String message);
    }
}