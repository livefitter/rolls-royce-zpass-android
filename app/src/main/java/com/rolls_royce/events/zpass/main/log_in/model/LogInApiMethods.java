package com.rolls_royce.events.zpass.main.log_in.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.UserModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public interface LogInApiMethods {

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_LOG_IN)
    Call<BaseResponseModel<UserModel>> logIn(@Body LogInRequestModel body);
}
