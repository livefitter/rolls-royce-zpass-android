package com.rolls_royce.events.zpass.data;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.main.scan_attendance.model.ScanAttendanceRequestModel;
import com.rolls_royce.events.zpass.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface LogOutApiMethods {

    @POST(AppConstants.URL_LOG_OUT)
    Call<BaseResponseModel> logOutUser(@Header("Authorization") String authToken);
}
