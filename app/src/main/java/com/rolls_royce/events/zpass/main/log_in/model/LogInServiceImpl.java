package com.rolls_royce.events.zpass.main.log_in.model;

import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class LogInServiceImpl implements LogInService {

    @Override
    public void logIn(LogInRequestModel body, final Callback callback) {
        LogInApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(LogInApiMethods.class);

        apiMethods.logIn(body).enqueue(new retrofit2.Callback<BaseResponseModel<UserModel>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<UserModel>> call, Response<BaseResponseModel<UserModel>> response) {
                LoggingHelper.debug(this, "onResponse", response);
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true data: " + response.body().getData());
                        callback.onSuccess(response.body().getData());
                    } else {
                        LoggingHelper.debug(this, "onResponse", "status false message: " + response.body().getMessage());
                        callback.onError(response.body().getMessage());
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<UserModel>> call, Throwable t) {
                t.printStackTrace();
                callback.onError(null);
            }
        });
    }
}
