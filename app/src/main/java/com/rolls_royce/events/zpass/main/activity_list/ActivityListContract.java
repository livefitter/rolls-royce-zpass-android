package com.rolls_royce.events.zpass.main.activity_list;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.EventActivityModel;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public interface ActivityListContract {

    public interface Presenter extends BasePresenter {
        void retrieveActivityList();
        int getActivityCount();
        void bindItemView(RepositoryItemView itemView, int index);
        void onActivityItemClick(int index);
    }

    public interface View extends BaseView<Presenter> {
        void updateActivityList();
        void toggleUIEnabled(boolean isEnabled);
        void toggleProgressVisibility(boolean isVisible);
        void toggleSwipeProgress(boolean isVisible);
        void showRetrieveActivityListError(String message);
        void proceedToAttendanceScreen(EventActivityModel activityModel);
    }

    public interface RepositoryItemView {
        void setName(String name);
        void setDate(String date);
        void setVenue(String venue);
    }
}
