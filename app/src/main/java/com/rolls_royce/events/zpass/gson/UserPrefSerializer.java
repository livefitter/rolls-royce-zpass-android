package com.rolls_royce.events.zpass.gson;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.rolls_royce.events.zpass.model.UserModel;

import java.lang.reflect.Type;


/**
 * Created by LloydM on 10/2/17
 * for Livefitter
 */

public class UserPrefSerializer implements JsonSerializer<UserModel> {
    @Override
    public JsonElement serialize(UserModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();

        if (!Strings.isNullOrEmpty(src.getEmail())) {
            jsonObject.addProperty("email", src.getEmail());
        }
        if (!Strings.isNullOrEmpty(src.getAuthToken())) {
            jsonObject.addProperty("authentication_token", src.getAuthToken());
        }

        jsonObject.addProperty("id", src.getId());

        return jsonObject;
    }
}
