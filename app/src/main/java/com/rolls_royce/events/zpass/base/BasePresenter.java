package com.rolls_royce.events.zpass.base;

/**
 * Created by LloydM on 8/18/17
 * for Livefitter
 */

public interface BasePresenter {

    /**
     * Called by the View to let the Presenter know that the UI is ready to receive and display data
     */
    void start();
}
