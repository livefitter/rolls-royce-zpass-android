package com.rolls_royce.events.zpass;

/**
 * Created by LFT-PC-010 on 7/12/2017.
 */

public class AppConstants {

    // -- URLs
    public static final String URL_BASE                                                     = BuildConfig.BASE_API_ENDPOINT;
    public static final String URL_API_VERSION                                              = "/api/v1";
    public static final String URL_LOG_IN                                                   = URL_API_VERSION + "/login?trainer=1";
    public static final String URL_LOG_OUT                                                  = URL_API_VERSION + "/logout";
    //todo remember to enable this to retrieve only the events for today
    public static final String URL_EVENTS                                                   = URL_API_VERSION + "/events?tag=1";
    // TESTING Get all events
    //public static final String URL_EVENTS                                                   = URL_API_VERSION + "/events";
    public static final String URL_ACTIVITIES                                               = URL_API_VERSION + "/activities/";
    public static final String URL_VERIFY_QR                                                = URL_API_VERSION + "/user_activities?scan=1";
    public static final String URL_MANUAL_REGISTRATION                                      = URL_API_VERSION + "/user_activities?manual=1";
    public static final String URL_MANUAL_CHECK_IN                                          = URL_API_VERSION + "/user_activities?checkin=1";
    public static final String URL_RANKING                                                  = URL_API_VERSION + "/user_points";

    // -- Fragment tags
    public static String FRAG_CURRENT;

    // -- Shared Preferences
    public static final String PREFS                                                        = "prefs";
    public static final String PREFS_USER                                                   = "user";
    public static final String PREFS_CURRENT_USER                                           = "current_user";

    // -- Bundle tags
    public static final String BUNDLE_EVENT_ID                                              = "event_id";
    public static final String BUNDLE_ACTIVITY                                              = "activity";

    // -- String Variables


    // -- Activity Requests
    public static final int REQUEST_SCAN_ATTENDANCE                                         = 0x222;
    public static final int REQUEST_MANUAL_REGISTRATION                                     = 0x223;
    public static final int REQUEST_MANUAL_CHECK_IN                                         = 0x224;

    // -- API server responses
    public static final String API_RESPONSE_INVALID_AUTH                                    = "Invalid authentication token";

    // -- QR Scanning variables
    public static final int SCAN_STATUS_DISABLED                                            = 0x788;
    public static final int SCAN_STATUS_FAILED                                              = 0x789;
    public static final int SCAN_STATUS_READY                                               = 0x790;
    public static final int SCAN_STATUS_VALIDATING                                          = 0x791;
}
