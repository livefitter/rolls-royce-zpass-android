package com.rolls_royce.events.zpass.main.attendance.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;
import com.rolls_royce.events.zpass.model.EventActivityModel;


/**
 * Created by LloydM on 11/16/17
 * for Livefitter
 */

public interface RetrieveActivityDetailsService {
    void retrieveActivityDetail(String authToken,
                                int activityId,
                                final Callback callback);

    public interface Callback extends BaseServiceCallback {

        void onSuccess(EventActivityModel activityModel);

        void onError(@Nullable String message);
    }
}