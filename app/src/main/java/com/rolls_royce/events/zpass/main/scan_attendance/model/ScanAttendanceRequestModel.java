package com.rolls_royce.events.zpass.main.scan_attendance.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ScanAttendanceRequestModel {

    private Payload data;

    public ScanAttendanceRequestModel(int userId, int activityId) {
        this.data = new Payload(userId, activityId);
    }

    private class Payload{
        @SerializedName("user_id")
        private int userId;
        @SerializedName("activity_id")
        private int activityId;

        public Payload(int userId, int activityId) {
            this.userId = userId;
            this.activityId = activityId;
        }
    }
}
