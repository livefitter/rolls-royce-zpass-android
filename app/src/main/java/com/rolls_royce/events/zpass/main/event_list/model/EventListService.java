package com.rolls_royce.events.zpass.main.event_list.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;
import com.rolls_royce.events.zpass.model.EventModel;

import java.util.ArrayList;


/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public interface EventListService {
    void retrieveEvents(String authToken,
                        final RetrieveEventListCallback callback);

    public interface RetrieveEventListCallback extends BaseServiceCallback {

        void onRetrieveEventsSuccess(ArrayList<EventModel> eventList);

        void onRetrieveEventsError(@Nullable String message);
    }
}