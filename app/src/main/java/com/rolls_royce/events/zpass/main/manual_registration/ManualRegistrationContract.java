package com.rolls_royce.events.zpass.main.manual_registration;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface ManualRegistrationContract {

    static final String KEY_NAME = "name";
    static final String KEY_EMAIL = "email";
    static final String KEY_EMPLOYEE_ID = "employee_id";

    public interface Presenter extends BasePresenter {
        void onIndemnityCheckboxChecked(boolean isChecked);

        void confirmRegistration(HashMap<String, String> leaderMap, @Nullable ArrayList<HashMap<String, String>> memberMapList);

        void onSuccessDialogDismissed();
    }

    public interface View extends BaseView<Presenter> {
        void toggleTeamLeaderLabelVisibility(boolean isVisible);

        void toggleTeamMemberLayoutVisibility(boolean isVisible);

        void toggleConfirmButtonEnabled(boolean isEnabled);

        void addTeamMemberEntryFields(int teamMemberCount);

        void showRequiredLeaderNameError();

        void showRequiredLeaderEmailError();

        void showRequiredLeaderEmployeIdError();

        void showInvalidLeaderEmailError();

        void showRequiredMemberNameError(int index);

        void showRequiredMemberEmailError(int index);

        void showRequiredMemberEmployeeIdError(int index);

        void showInvalidMemberEmailError(int index);

        void showRegistrationSuccess(String message);

        void showRegistrationError(String message);

        void toggleUIEnabled(boolean isEnabled);

        void toggleProgressVisibility(boolean isVisible);

        void proceedToScanningAttendanceActivity();
    }
}
