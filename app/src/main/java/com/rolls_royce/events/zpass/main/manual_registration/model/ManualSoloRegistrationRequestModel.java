package com.rolls_royce.events.zpass.main.manual_registration.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualSoloRegistrationRequestModel {

    private Payload data;

    public ManualSoloRegistrationRequestModel(int activityId, String name, String employeeId, String email){
        data = new Payload(activityId, name, employeeId, email);
    }

    private class Payload{

        @SerializedName("activity_id")
        private int activityId;
        private String name;
        @SerializedName("employee_id")
        private String employeeId;
        private String email;

        public Payload(int activityId, String name, String employeeId, String email) {
            this.activityId = activityId;
            this.name = name;
            this.employeeId = employeeId;
            this.email = email;
        }
    }
}
