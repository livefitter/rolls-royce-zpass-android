package com.rolls_royce.events.zpass.main.scan_attendance.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ScanAttendanceServiceImpl implements ScanAttendanceService {

    @Override
    public void validateQrCode(String authToken, ScanAttendanceRequestModel body, final validateQrCodeCallback callback) {
        ScanAttendanceApiMethods apiMethods = RetrofitUtil.getGSONRetrofit().create(ScanAttendanceApiMethods.class);

        apiMethods.verifyQrCode(authToken, body).enqueue(new Callback<BaseResponseModel>() {
            @Override
            public void onResponse(Call<BaseResponseModel> call, Response<BaseResponseModel> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        LoggingHelper.debug(this, "onResponse", "status true message: " + response.body().getMessage());
                        callback.onValidateQrCodeSuccess(response.body().getMessage());
                    } else {
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if (response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)) {
                            callback.onInvalidAuthenticationError(message);
                        } else {
                            callback.onValidateQrCodeError(message);
                        }
                    }
                } else {
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onValidateQrCodeError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onValidateQrCodeError(null);
            }
        });
    }
}
