package com.rolls_royce.events.zpass.utility;

import android.util.Log;

import com.rolls_royce.events.zpass.BuildConfig;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class LoggingHelper {
    public static void debug(Object object, String tag, String message){
        if (BuildConfig.DEBUG) {
            String logMessage = tag != null && !tag.isEmpty()? tag + " : " + message : message;
            Log.d(object.getClass().getSimpleName(), logMessage);
        }
    }

    public static void debug(Object object, String message){
        debug(object, null, message);
    }

    public static void debug(Object object, String tag, Object message){
        if (BuildConfig.DEBUG) {
            String messageString = message.toString();
            String logMessage = tag != null && !tag.isEmpty()? tag + " : " + messageString : messageString;
            Log.d(object.getClass().getSimpleName(), logMessage);
        }
    }

    public static void debug(Object object, Object message){
        debug(object, null, message);
    }
}
