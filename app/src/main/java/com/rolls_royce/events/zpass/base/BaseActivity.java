package com.rolls_royce.events.zpass.base;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.main.log_in.view.LogInActivity;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

/**
 * Created by LFT-PC-010 on 7/12/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected boolean isActivityActive;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        isActivityActive = true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isActivityActive = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActivityActive = true;
    }

    @Override
    protected void onDestroy() {
        isActivityActive = false;
        super.onDestroy();
    }

    /**
     * Start another activity
     *
     * @param cls          Activity class
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Class<?> cls, boolean shouldFinish) {

        Intent i = new Intent(this, cls);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        switchActivity(i, shouldFinish);
    }

    /**
     * Start another activity
     *
     * @param cls          Activity class
     * @param bundleKey    Key for bundle extras
     * @param bundleExtras
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Class<?> cls, String bundleKey, Bundle bundleExtras, boolean shouldFinish) {

        Intent i = new Intent(this, cls);
        i.putExtra(bundleKey, bundleExtras);

        switchActivity(i, shouldFinish);
    }

    /**
     * Start another activity
     *
     * @param intent       custom intent
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Intent intent, boolean shouldFinish) {

        startActivity(intent);

        if (shouldFinish) {
            finish();
        }
    }

    /**
     * Replace the fragment in the given container
     *
     * @param fragment       The fragment to show
     * @param container      Resource ID for the container
     * @param fragmentTag    Fragment tag
     * @param addToBackStack Add this fragment to the backstack
     * @param clearBackStack Clear the backstack prior to adding the fragment
     */
    public void switchFragment(Fragment fragment, @IdRes int container, String fragmentTag, boolean addToBackStack, boolean clearBackStack) {

        AppConstants.FRAG_CURRENT = fragmentTag;

        if (clearBackStack) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (addToBackStack) {
            getSupportFragmentManager().beginTransaction().replace(container, fragment, fragmentTag).addToBackStack(fragmentTag).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(container, fragment, fragmentTag).commit();
        }
    }

    public void showSoftKeyboard(View editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void hideSoftKeyboard(View view) {
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void logOutUser() {
        SharedPrefsUtil.clearUserPrefs(this);

        Intent intent = new Intent(this, LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected boolean isActivityActive() {
        return isActivityActive;
    }

    /**
     * Convenience method to easily display a confirmation message whenever the user wants to logout
     */
    public void showLogOutDialog() {

        DialogUtil.showConfirmationDialog(this,
                getString(R.string.label_log_out),
                getString(R.string.msg_confirm_log_out),
                getString(android.R.string.yes),
                getString(android.R.string.no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            logOutUser();
                        }
                    }
                });
    }
}
