package com.rolls_royce.events.zpass.main.manual_check_in.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;


/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public interface ManualCheckInService {
    void checkInUser(String authToken,
                     ManualCheckInRequestModel body,
                     final Callback callback);

    public interface Callback extends BaseServiceCallback {

        void onCheckInSuccess(String message);

        void onCheckInError(@Nullable String message);
    }
}