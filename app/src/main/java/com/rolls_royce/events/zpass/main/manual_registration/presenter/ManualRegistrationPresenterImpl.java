package com.rolls_royce.events.zpass.main.manual_registration.presenter;

import android.support.annotation.Nullable;
import android.util.Patterns;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.main.manual_registration.ManualRegistrationContract;
import com.rolls_royce.events.zpass.main.manual_registration.model.ManualRegistrationService;
import com.rolls_royce.events.zpass.main.manual_registration.model.ManualRegistrationServiceImpl;
import com.rolls_royce.events.zpass.main.manual_registration.model.ManualSoloRegistrationRequestModel;
import com.rolls_royce.events.zpass.main.manual_registration.model.ManualTeamRegistrationRequestModel;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public class ManualRegistrationPresenterImpl implements ManualRegistrationContract.Presenter, ManualRegistrationService.Callback {

    private ManualRegistrationContract.View mView;
    private UserModel mUserModel;
    private EventActivityModel mActivityModel;
    private ManualRegistrationService mService;


    public ManualRegistrationPresenterImpl(UserModel userModel, EventActivityModel activityModel, ManualRegistrationContract.View view) {
        this.mView = view;
        mUserModel = userModel;
        mActivityModel = activityModel;
        mService = new ManualRegistrationServiceImpl();
        mView.setPresenter(this);
    }

    private void prepareUI() {
        // Determine if we are booking for an individual or for a team
        mView.toggleTeamLeaderLabelVisibility(mActivityModel.isTeamActivity());
        mView.toggleTeamMemberLayoutVisibility(mActivityModel.isTeamActivity());

        // Create team member fields based on the required number of team members minus the leader
        if (mActivityModel.isTeamActivity()) {
            mView.addTeamMemberEntryFields(mActivityModel.getRequiredMemberCount() - 1);
        }

        // If the indemnity checkbox is mandatory by virtue of being a team activity
        // or it's an individual activity
        // disable the confirmation button
        if ((mActivityModel.isTeamActivity() && mActivityModel.isSurveyMandatory())
                || !mActivityModel.isTeamActivity()) {
            mView.toggleConfirmButtonEnabled(false);
        }
    }

    private boolean validateFields(HashMap<String, String> leaderMap, @Nullable ArrayList<HashMap<String, String>> memberMapList) {
        boolean areFieldsValid = true;

        // Leader fields
        String name = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_NAME));
        String email = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_EMAIL));
        String employeeId = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_EMPLOYEE_ID));

        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            areFieldsValid = false;
            mView.showRequiredLeaderNameError();
        }

        if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(email.trim())) {
            areFieldsValid = false;
            mView.showRequiredLeaderEmailError();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            areFieldsValid = false;
            mView.showInvalidLeaderEmailError();
        }

        if (Strings.isNullOrEmpty(employeeId) || Strings.isNullOrEmpty(employeeId.trim())) {
            areFieldsValid = false;
            mView.showRequiredLeaderEmployeIdError();
        }

        // Member fields
        if (mActivityModel.isTeamActivity() && memberMapList != null) {
            for (int i = 0; i < memberMapList.size(); i++) {
                HashMap<String, String> memberMap = memberMapList.get(i);

                name = Strings.nullToEmpty(memberMap.get(ManualRegistrationContract.KEY_NAME));
                email = Strings.nullToEmpty(memberMap.get(ManualRegistrationContract.KEY_EMAIL));
                employeeId = Strings.nullToEmpty(memberMap.get(ManualRegistrationContract.KEY_EMPLOYEE_ID));

                if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
                    areFieldsValid = false;
                    mView.showRequiredMemberNameError(i);
                }

                if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(email.trim())) {
                    areFieldsValid = false;
                    mView.showRequiredMemberEmailError(i);
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    areFieldsValid = false;
                    mView.showInvalidMemberEmailError(i);
                }

                if (Strings.isNullOrEmpty(employeeId) || Strings.isNullOrEmpty(employeeId.trim())) {
                    areFieldsValid = false;
                    mView.showRequiredMemberEmployeeIdError(i);
                }
            }
        }

        return areFieldsValid;
    }

    private void doRegister(HashMap<String, String> leaderMap, @Nullable ArrayList<HashMap<String, String>> memberMapList) {
        if (mActivityModel.isTeamActivity() && memberMapList != null && !memberMapList.isEmpty()) {
            memberMapList.add(0, leaderMap);

            ArrayList<HashMap<String, String>> requestModelMapList = new ArrayList<>();

            // Convert
            for (int i = 0; i < memberMapList.size(); i++) {
                HashMap<String, String> memberMap = memberMapList.get(i);
                HashMap<String, String> requestModelMap = new HashMap<>();

                requestModelMap.put(ManualTeamRegistrationRequestModel.KEY_NAME, memberMap.get(ManualRegistrationContract.KEY_NAME));
                requestModelMap.put(ManualTeamRegistrationRequestModel.KEY_EMAIL, memberMap.get(ManualRegistrationContract.KEY_EMAIL));
                requestModelMap.put(ManualTeamRegistrationRequestModel.KEY_EMPLOYEE_ID, memberMap.get(ManualRegistrationContract.KEY_EMPLOYEE_ID));

                requestModelMapList.add(requestModelMap);
            }

            ManualTeamRegistrationRequestModel body = new ManualTeamRegistrationRequestModel(mActivityModel.getId(),
                    requestModelMapList);

            mService.teamRegistration(mUserModel.getAuthToken(),
                    body,
                    this);
        } else {

            String name = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_NAME));
            String email = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_EMAIL));
            String employeeId = Strings.nullToEmpty(leaderMap.get(ManualRegistrationContract.KEY_EMPLOYEE_ID));

            ManualSoloRegistrationRequestModel body = new ManualSoloRegistrationRequestModel(mActivityModel.getId(),
                    name,
                    employeeId,
                    email);

            mService.soloRegistration(mUserModel.getAuthToken(),
                    body,
                    this);
        }
    }

    @Override
    public void start() {
        prepareUI();
    }

    @Override
    public void onIndemnityCheckboxChecked(boolean isChecked) {
        if ((mActivityModel.isTeamActivity() && mActivityModel.isSurveyMandatory())
                || !mActivityModel.isTeamActivity()) {
            mView.toggleConfirmButtonEnabled(isChecked);
        }
    }

    @Override
    public void confirmRegistration(HashMap<String, String> leaderMap, @Nullable ArrayList<HashMap<String, String>> memberMapList) {
        if (validateFields(leaderMap, memberMapList)) {
            mView.toggleProgressVisibility(true);
            mView.toggleUIEnabled(false);

            doRegister(leaderMap, memberMapList);
        }
    }

    @Override
    public void onSuccessDialogDismissed() {
        mView.proceedToScanningAttendanceActivity();
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.onInvalidAuthToken(message);
        }
    }

    @Override
    public void onRegistrationSuccess(String message) {

        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleUIEnabled(true);

            mView.showRegistrationSuccess(message);
        }
    }

    @Override
    public void onRegistrationError(@Nullable String message) {
        Preconditions.checkNotNull(mView);

        if (mView.isActive()) {
            mView.toggleProgressVisibility(false);
            mView.toggleUIEnabled(true);

            mView.showRegistrationError(message);
        }
    }
}