package com.rolls_royce.events.zpass.main.manual_registration.model;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.model.BaseResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface ManualRegistrationApiMethods {
    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_MANUAL_REGISTRATION)
    Call<BaseResponseModel> soloRegistration(@Header("Authorization") String authToken,
                                             @Body ManualSoloRegistrationRequestModel body);

    @Headers({
            "Accept: application/json",
            "Content-type: application/json"
    })
    @POST(AppConstants.URL_MANUAL_REGISTRATION)
    Call<BaseResponseModel> teamRegistration(@Header("Authorization") String authToken,
                                             @Body ManualTeamRegistrationRequestModel body);
}
