package com.rolls_royce.events.zpass.main.activity_list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.activity_list.ActivityListContract;
import com.rolls_royce.events.zpass.main.activity_list.adapter.ActivityListAdapter;
import com.rolls_royce.events.zpass.main.activity_list.presenter.ActivityListPresenterImpl;
import com.rolls_royce.events.zpass.main.attendance.view.AttendanceActivity;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityListActivity extends BaseActivity implements View.OnClickListener, ActivityListContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout flProgressBarLayout;
    @BindView(R.id.recycler_view)
    RecyclerView rvActivities;

    private ActivityListContract.Presenter mPresenter;
    private ActivityListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize(){
        String title = getString(R.string.label_activities).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        rvActivities.setLayoutManager(layoutManager);
        rvActivities.addItemDecoration(itemDecoration);

        flProgressBarLayout.setBackgroundResource(R.color.white);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) flProgressBarLayout.getLayoutParams();
        layoutParams.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        flProgressBarLayout.setLayoutParams(layoutParams);
        flProgressBarLayout.requestLayout();

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter != null) {
                    mPresenter.retrieveActivityList();
                }
            }
        });

        UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(AppConstants.BUNDLE_EVENT_ID)) {

            int eventId = extras.getInt(AppConstants.BUNDLE_EVENT_ID, 0);

            if(eventId != 0){

                if (userModel != null) {
                    new ActivityListPresenterImpl(userModel, eventId, this);
                }

                if (mPresenter != null) {
                    mAdapter = new ActivityListAdapter(this, mPresenter, this);
                    rvActivities.setAdapter(mAdapter);

                    mPresenter.start();
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_out) {
            showLogOutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int index = rvActivities.getChildAdapterPosition(view);

        if(index != RecyclerView.NO_POSITION && mPresenter != null){
            mPresenter.onActivityItemClick(index);
        }
    }

    @Override
    public void setPresenter(ActivityListContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public void updateActivityList() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {
        rvActivities.setEnabled(isEnabled);
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        if (flProgressBarLayout != null) {
            int visibility = isVisible? View.VISIBLE : View.GONE;
            flProgressBarLayout.setVisibility(visibility);
        }
    }

    @Override
    public void toggleSwipeProgress(boolean isVisible) {
        if (srLayout != null) {
            srLayout.setRefreshing(isVisible);
        }
    }

    @Override
    public void showRetrieveActivityListError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_retrieve_activities);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void proceedToAttendanceScreen(EventActivityModel activityModel) {
        Intent intent = new Intent(this, AttendanceActivity.class);
        intent.putExtra(AppConstants.BUNDLE_ACTIVITY, activityModel);
        startActivity(intent);
    }
}
