package com.rolls_royce.events.zpass.main.manual_registration.model;

import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseServiceCallback;


/**
 * Created by LloydM on 11/21/17
 * for Livefitter
 */

public interface ManualRegistrationService {
    void soloRegistration(String authToken,
                          ManualSoloRegistrationRequestModel body,
                          final Callback callback);

    void teamRegistration(String authToken,
                          ManualTeamRegistrationRequestModel body,
                          final Callback callback);

    public interface Callback extends BaseServiceCallback {

        void onRegistrationSuccess(String message);

        void onRegistrationError(@Nullable String message);
    }
}