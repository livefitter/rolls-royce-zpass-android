package com.rolls_royce.events.zpass.main.log_in;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.UserModel;

/**
 * Created by LloydM on 11/8/17
 * for Livefitter
 */

public interface LogInContract {

    public interface Presenter extends BasePresenter {
        void logInUser(String email,
                       String password);
    }

    public interface View extends BaseView<Presenter> {
        void clearPasswordField();
        void showRequiredEmailError();
        void showRequiredPasswordError();
        void showInvalidEmailError();
        void toggleProgressVisibility(boolean isVisible);
        void toggleUIEnabled(boolean isEnabled);
        void saveUserData(UserModel userModel);
        void showLogInError(String message);
        void proceedToEventsScreen();
    }
}
