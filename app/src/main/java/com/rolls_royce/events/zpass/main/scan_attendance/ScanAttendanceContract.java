package com.rolls_royce.events.zpass.main.scan_attendance;

import com.journeyapps.barcodescanner.BarcodeCallback;
import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;
import com.rolls_royce.events.zpass.model.EventActivityModel;

/**
 * Created by LloydM on 11/20/17
 * for Livefitter
 */

public interface ScanAttendanceContract {

    public interface Presenter extends BasePresenter {
        void prepareScan();

        void resumeScan();

        void disableScan();

        void onRegistrationClick();

        void onCheckInClick();

        void onQRScanSuccess(String qrContent);

        void onQRScanFailed();

        void onTransactionRequestSuccess();

        /**
         * Control when to enable QR code decoding after the user has dismissed the dialog to avoid potential errors
         */
        void onDialogMessageDismissed();

        /**
         * Becomes true if the user has successfully scanned a QR code,
         * or manually registered,
         * or manually checked in once
         */
        boolean hasSuccessfullyTransacted();
    }

    public interface View extends BaseView<Presenter> {

        /**
         * Method to mRequest permission from the user to use the device's camera.
         * @return if permission to use the Camera has been allowed
         */
        boolean mayUseCamera();

        void toggleProgressVisibility(boolean isVisible);

        void toggleUIEnabled(boolean isEnabled);

        void toggleEnableScanning(boolean enableScan, boolean resumeOngoingScan);

        void togglePauseScanning(boolean pauseScan);

        /**
         * Set the status text of the barcode scanner
         *
         * @param scanStatus can be {@link com.rolls_royce.events.zpass.AppConstants#SCAN_STATUS_DISABLED disabled},
         *                   {@link com.rolls_royce.events.zpass.AppConstants#SCAN_STATUS_READY ready to scan},
         *                   {@link com.rolls_royce.events.zpass.AppConstants#SCAN_STATUS_FAILED failed to recognize the QR code},
         *                   or {@link com.rolls_royce.events.zpass.AppConstants#SCAN_STATUS_VALIDATING successful and currently validating the QR code}.
         */
        void setScanStatus(int scanStatus);

        void showValidQrCodeDialog(String message);

        void showInvalidQrCodeError(String message);

        void setTotalScanCount(int totalScanCount);

        void proceedToRegistrationScreen(EventActivityModel activityModel);

        void proceedToCheckInScreen(EventActivityModel activityModel);
    }
}
