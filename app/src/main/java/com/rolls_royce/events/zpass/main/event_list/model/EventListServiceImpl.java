package com.rolls_royce.events.zpass.main.event_list.model;

import android.util.Log;

import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.data.EventActivitiesApiMethods;
import com.rolls_royce.events.zpass.model.BaseResponseModel;
import com.rolls_royce.events.zpass.model.EventModel;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.RetrofitUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class EventListServiceImpl implements EventListService {
    private EventActivitiesApiMethods mApiMethods;

    private void initialize(){
        if(mApiMethods == null) {
            mApiMethods = RetrofitUtil.getGSONRetrofit().create(EventActivitiesApiMethods.class);
        }
    }

    @Override
    public void retrieveEvents(String authToken, final RetrieveEventListCallback callback) {
        initialize();
        mApiMethods.retrieveEvents(authToken).enqueue(new Callback<BaseResponseModel<ArrayList<EventModel>>>() {
            @Override
            public void onResponse(Call<BaseResponseModel<ArrayList<EventModel>>> call, Response<BaseResponseModel<ArrayList<EventModel>>> response) {
                LoggingHelper.debug(this, "onResponse", response);

                if(response.isSuccessful()){
                    if(response.body().getStatus()){
                        LoggingHelper.debug(this, "onResponse", "status true data: " + response.body().getData());
                        callback.onRetrieveEventsSuccess(response.body().getData());
                    }else{
                        String message = response.body().getMessage();
                        LoggingHelper.debug(this, "onResponse", "status false message: " + message);
                        if(response.body().getMessage().contains(AppConstants.API_RESPONSE_INVALID_AUTH)){
                            callback.onInvalidAuthenticationError(message);
                        }else {
                            callback.onRetrieveEventsError(message);
                        }
                    }
                }else{
                    LoggingHelper.debug(this, "onResponse", "failed");
                    callback.onRetrieveEventsError(null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponseModel<ArrayList<EventModel>>> call, Throwable t) {
                t.printStackTrace();
                callback.onRetrieveEventsError(null);
            }
        });
    }
}
