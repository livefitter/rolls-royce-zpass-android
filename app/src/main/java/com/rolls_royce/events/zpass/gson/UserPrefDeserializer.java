package com.rolls_royce.events.zpass.gson;

import com.google.common.base.Strings;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.rolls_royce.events.zpass.model.UserModel;

import java.lang.reflect.Type;


/**
 * Created by LloydM on 10/2/17
 * for Livefitter
 */

public class UserPrefDeserializer implements JsonDeserializer<UserModel> {
    @Override
    public UserModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        int id = jsonObject.get("id").getAsInt();
        String authToken = jsonObject.get("authentication_token").getAsString();
        String email = jsonObject.get("email").getAsString();

        if(!Strings.isNullOrEmpty(authToken) && !Strings.isNullOrEmpty(email)){
            return new UserModel(id, email, authToken);
        }

        return null;
    }
}
