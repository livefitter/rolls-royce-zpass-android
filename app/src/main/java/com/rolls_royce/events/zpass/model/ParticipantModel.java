package com.rolls_royce.events.zpass.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


/**
 * Created by LloydM on 11/17/17
 * for Livefitter
 */

public class ParticipantModel implements Parcelable {


    /**
     * status : Confirmed
     * user_id : 2
     * name : Dummy Account
     * email : dummy@gmail.com
     * house : House Bravo
     * team_id : 10555
     * status_id: 1,
     * rank: "1st"
     */

    private String status;
    @SerializedName("user_id")
    private int userId;
    private String name;
    private String email;
    private String house;
    @SerializedName("team_id")
    private int teamId;
    @SerializedName("status_id")
    private int statusId;
    private String rank;
    @SerializedName("is_leader")
    private boolean isLeader;

    protected ParticipantModel(Parcel in) {
        status = in.readString();
        userId = in.readInt();
        name = in.readString();
        email = in.readString();
        house = in.readString();
        teamId = in.readInt();
        statusId = in.readInt();
        rank = in.readString();
        isLeader = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeInt(userId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(house);
        dest.writeInt(teamId);
        dest.writeInt(statusId);
        dest.writeString(rank);
        dest.writeByte((byte) (isLeader ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ParticipantModel> CREATOR = new Creator<ParticipantModel>() {
        @Override
        public ParticipantModel createFromParcel(Parcel in) {
            return new ParticipantModel(in);
        }

        @Override
        public ParticipantModel[] newArray(int size) {
            return new ParticipantModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getHouse() {
        return house;
    }

    public int getTeamId() {
        return teamId;
    }

    public int getStatusId() {
        return statusId;
    }

    public String getRank() {
        return rank;
    }

    public boolean isLeader() {
        return isLeader;
    }
}
