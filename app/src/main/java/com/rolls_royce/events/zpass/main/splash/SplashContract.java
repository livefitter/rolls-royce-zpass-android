package com.rolls_royce.events.zpass.main.splash;

import com.rolls_royce.events.zpass.base.BasePresenter;
import com.rolls_royce.events.zpass.base.BaseView;

/**
 * Created by LloydM on 11/8/17
 * for Livefitter
 */

public interface SplashContract {

    public interface Presenter extends BasePresenter {
    }

    public interface View extends BaseView<Presenter> {
        void proceedToLogInScreen();
        void proceedToEventsScreen();
    }
}
