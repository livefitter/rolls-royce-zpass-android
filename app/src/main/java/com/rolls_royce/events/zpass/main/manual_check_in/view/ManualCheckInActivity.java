package com.rolls_royce.events.zpass.main.manual_check_in.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.manual_check_in.ManualCheckInContract;
import com.rolls_royce.events.zpass.main.manual_check_in.presenter.ManualCheckInPresenterImpl;
import com.rolls_royce.events.zpass.main.scan_attendance.view.ScanAttendanceActivity;
import com.rolls_royce.events.zpass.model.EventActivityModel;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.FormFieldTextWatcher;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by LloydM on 11/22/17
 * for Livefitter
 */

public class ManualCheckInActivity extends BaseActivity implements ManualCheckInContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.et_email)
    TextInputEditText etEmail;
    @BindView(R.id.cb_indemnity)
    CheckBox cbIndemnity;
    @BindView(R.id.btn_check_in)
    Button btnCheckIn;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout mProgressBarLayout;

    private ManualCheckInContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_check_in);
        ButterKnife.bind(this);
        initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_out) {
            showLogOutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        String title = getString(R.string.label_manual_check_in).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));

        cbIndemnity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (mPresenter != null) {
                    mPresenter.onIndemnityCheckboxChecked(isChecked);
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.BUNDLE_ACTIVITY)) {
            UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
            EventActivityModel activityModel = extras.getParcelable(AppConstants.BUNDLE_ACTIVITY);

            if (userModel != null && activityModel != null) {
                new ManualCheckInPresenterImpl(userModel, activityModel, this);
            }
        }

        if (mPresenter != null) {
            mPresenter.start();
        }
    }

    @OnClick(R.id.btn_check_in)
    public void checkInClick() {
        if (mPresenter != null) {
            mPresenter.checkInUser(etEmail.getText().toString());
        }
    }

    @Override
    public void setPresenter(ManualCheckInContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public void toggleCheckInButtonEnabled(boolean isEnabled) {
        btnCheckIn.setEnabled(isEnabled);
    }

    @Override
    public void showRequiredEmailError() {
        String fieldName = getString(R.string.label_email);
        String message = getString(R.string.error_required_field, fieldName);
        tilEmail.setError(message);
    }

    @Override
    public void showInvalidEmailError() {
        String message = getString(R.string.error_invalid_email);
        tilEmail.setError(message);
    }

    @Override
    public void showCheckInSuccess(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (mPresenter != null) {
                    mPresenter.onSuccessDialogDismissed();
                }
            }
        });
    }

    @Override
    public void showCheckInError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_check_in);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {
        tilEmail.setEnabled(isEnabled);
        etEmail.setEnabled(isEnabled);
        cbIndemnity.setEnabled(isEnabled);
        btnCheckIn.setEnabled(isEnabled);
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;
        mProgressBarLayout.setVisibility(visibility);
    }

    @Override
    public void proceedToScanningAttendanceActivity() {
        setResult(RESULT_OK, new Intent(this, ScanAttendanceActivity.class));
        finish();
    }
}
