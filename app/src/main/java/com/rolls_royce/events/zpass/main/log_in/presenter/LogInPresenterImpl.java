package com.rolls_royce.events.zpass.main.log_in.presenter;

import android.support.annotation.Nullable;
import android.util.Patterns;

import com.google.common.base.Preconditions;
import com.rolls_royce.events.zpass.main.log_in.LogInContract;
import com.rolls_royce.events.zpass.main.log_in.model.LogInRequestModel;
import com.rolls_royce.events.zpass.main.log_in.model.LogInService;
import com.rolls_royce.events.zpass.main.log_in.model.LogInServiceImpl;
import com.rolls_royce.events.zpass.model.UserModel;

/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class LogInPresenterImpl implements LogInContract.Presenter, LogInService.Callback {

    private LogInContract.View mView;
    private LogInService mService;

    public LogInPresenterImpl(LogInContract.View view) {
        this.mView = view;
        this.mService = new LogInServiceImpl();
        mView.setPresenter(this);
    }

    private boolean verifyFields(String email, String password) {
        boolean areFieldsValid = true;

        if (email == null || email.trim().isEmpty()) {
            areFieldsValid = false;
            mView.showRequiredEmailError();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            areFieldsValid = false;
            mView.showInvalidEmailError();
        }

        if (password == null || password.trim().isEmpty()) {
            areFieldsValid = false;
            mView.showRequiredPasswordError();
        }

        return areFieldsValid;
    }

    @Override
    public void start() {
        //do nothing
    }

    @Override
    public void logInUser(String email, String password) {
        if (verifyFields(email, password)) {
            mView.toggleUIEnabled(false);
            mView.toggleProgressVisibility(true);

            mService.logIn(new LogInRequestModel(email, password), this);
        }
    }

    @Override
    public void onInvalidAuthenticationError(@Nullable String message) {
        //do nothing
    }

    @Override
    public void onSuccess(UserModel userModel) {
        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.toggleUIEnabled(true);
            mView.toggleProgressVisibility(false);
            mView.saveUserData(userModel);

            mView.proceedToEventsScreen();
        }
    }

    @Override
    public void onError(@Nullable String message) {
        Preconditions.checkNotNull(mView);
        if (mView.isActive()) {
            mView.toggleUIEnabled(true);
            mView.toggleProgressVisibility(false);
            mView.clearPasswordField();
            mView.showLogInError(message);
        }
    }
}