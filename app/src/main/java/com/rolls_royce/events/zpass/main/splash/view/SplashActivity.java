package com.rolls_royce.events.zpass.main.splash.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.event_list.view.EventListActivity;
import com.rolls_royce.events.zpass.main.log_in.view.LogInActivity;
import com.rolls_royce.events.zpass.main.splash.SplashContract;
import com.rolls_royce.events.zpass.main.splash.presenter.SplashPresenterImpl;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;


/**
 * Created by LloydM on 11/10/17
 * for Livefitter
 */

public class SplashActivity extends BaseActivity implements SplashContract.View {

    private SplashContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UserModel userModel = SharedPrefsUtil.getCurrentUser(this);

        new SplashPresenterImpl(userModel, this);

        if(mPresenter != null){
            mPresenter.start();
        }

    }

    @Override
    public void setPresenter(SplashContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        //do nothing
    }

    @Override
    public void proceedToLogInScreen() {
        switchActivity(new Intent(this, LogInActivity.class), true);
    }

    @Override
    public void proceedToEventsScreen() {
        switchActivity(new Intent(this, EventListActivity.class), true);
    }
}
