package com.rolls_royce.events.zpass.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


/**
 * Created by LloydM on 10/11/17
 * for Livefitter
 */

public class EventModel implements Parcelable {

    /**
     * id : 1
     * image_url : some-URL
     * name : Event A
     * description : some-text
     * venue : Singapore
     * date_time : 31 Oct 2017, 09:00AM - 06:00PM
     */

    private int id;
    @SerializedName("image_url")
    private String imageUrl;
    private String name;
    private String description;
    private String venue;
    @SerializedName("date_time")
    private String dateTime;

    protected EventModel(Parcel in) {
        id = in.readInt();
        imageUrl = in.readString();
        name = in.readString();
        description = in.readString();
        venue = in.readString();
        dateTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(imageUrl);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(venue);
        dest.writeString(dateTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EventModel) {
            EventModel other = (EventModel) obj;

            return this.getId() == other.getId();
        }
        return false;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
