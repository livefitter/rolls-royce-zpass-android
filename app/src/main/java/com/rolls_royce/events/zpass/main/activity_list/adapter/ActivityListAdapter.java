package com.rolls_royce.events.zpass.main.activity_list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rolls_royce.events.zpass.main.activity_list.ActivityListContract;


/**
 * Created by LloydM on 11/9/17
 * for Livefitter
 */

public class ActivityListAdapter extends RecyclerView.Adapter<ActivityItemViewHolder> {

    private final View.OnClickListener mClickListener;
    LayoutInflater mInflater;
    ActivityListContract.Presenter mPresenter;

    public ActivityListAdapter(Context context, ActivityListContract.Presenter presenter, View.OnClickListener clickListener){
        mInflater = LayoutInflater.from(context);
        this.mPresenter = presenter;
        this.mClickListener = clickListener;
    }

    @Override
    public ActivityItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ActivityItemViewHolder.create(mInflater, parent, mClickListener);
    }

    @Override
    public void onBindViewHolder(ActivityItemViewHolder holder, int position) {
        if (mPresenter != null) {
            mPresenter.bindItemView(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mPresenter != null? mPresenter.getActivityCount() : 0;
    }
}
