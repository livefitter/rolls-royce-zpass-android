package com.rolls_royce.events.zpass.main.event_list.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.common.base.Strings;
import com.rolls_royce.events.zpass.AppConstants;
import com.rolls_royce.events.zpass.R;
import com.rolls_royce.events.zpass.base.BaseActivity;
import com.rolls_royce.events.zpass.main.activity_list.view.ActivityListActivity;
import com.rolls_royce.events.zpass.main.event_list.EventListContract;
import com.rolls_royce.events.zpass.main.event_list.adapter.EventListAdapter;
import com.rolls_royce.events.zpass.main.event_list.presenter.EventListPresenterImpl;
import com.rolls_royce.events.zpass.model.UserModel;
import com.rolls_royce.events.zpass.utility.DialogUtil;
import com.rolls_royce.events.zpass.utility.LoggingHelper;
import com.rolls_royce.events.zpass.utility.SharedPrefsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventListActivity extends BaseActivity implements View.OnClickListener, EventListContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout srLayout;
    @BindView(R.id.fl_progress_bar_layout)
    FrameLayout flProgressBarLayout;
    @BindView(R.id.recycler_view)
    RecyclerView rvEvents;

    private EventListContract.Presenter mPresenter;
    private EventListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize(){
        String title = getString(R.string.label_events).toUpperCase();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(title);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        rvEvents.setLayoutManager(layoutManager);
        rvEvents.addItemDecoration(itemDecoration);

        flProgressBarLayout.setBackgroundResource(R.color.white);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) flProgressBarLayout.getLayoutParams();
        layoutParams.setBehavior(new AppBarLayout.ScrollingViewBehavior());
        flProgressBarLayout.setLayoutParams(layoutParams);
        flProgressBarLayout.requestLayout();

        srLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter != null) {
                    mPresenter.retrieveEventList();
                }
            }
        });

        UserModel userModel = SharedPrefsUtil.getCurrentUser(this);
        if (userModel != null) {
            new EventListPresenterImpl(userModel, this);
        }

        if (mPresenter != null) {
            mAdapter = new EventListAdapter(this, mPresenter, this);
            rvEvents.setAdapter(mAdapter);

            mPresenter.start();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_out) {
            showLogOutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int index = rvEvents.getChildAdapterPosition(view);

        if(index != RecyclerView.NO_POSITION && mPresenter != null){
            mPresenter.onEventItemClick(index);
        }
    }

    @Override
    public void setPresenter(EventListContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public boolean isActive() {
        return isActivityActive();
    }

    @Override
    public void onInvalidAuthToken(String message) {
        DialogUtil.showAlertDialog(this, message, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                logOutUser();
            }
        });
    }

    @Override
    public void updateEventList() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void toggleUIEnabled(boolean isEnabled) {
        rvEvents.setEnabled(isEnabled);
    }

    @Override
    public void toggleProgressVisibility(boolean isVisible) {
        if (flProgressBarLayout != null) {
            int visibility = isVisible? View.VISIBLE : View.GONE;
            flProgressBarLayout.setVisibility(visibility);
        }
    }

    @Override
    public void toggleSwipeProgress(boolean isVisible) {
        if (srLayout != null) {
            srLayout.setRefreshing(isVisible);
        }
    }

    @Override
    public void showRetrieveEventListError(String message) {
        if (Strings.isNullOrEmpty(message)) {
            message = getString(R.string.error_retrieve_events);
        }
        DialogUtil.showAlertDialog(this, message);
    }

    @Override
    public void proceedToActivityListScreen(int eventId) {
        Intent intent = new Intent(this, ActivityListActivity.class);
        intent.putExtra(AppConstants.BUNDLE_EVENT_ID, eventId);
        startActivity(intent);
    }
}
